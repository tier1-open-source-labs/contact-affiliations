Account Contact Relation Cube
=============================
This cube allows a user to set relations between Accounts and Contacts via the AccountContactRelation object. 

- allows user to add, edit, or remove these ACRs. 
- can only set non-primary ACR.
- able to show custom fields of the ACR object. 
- respects user's permission for the ACR object. 
- caches user's grid settings (width, ordering) in the AFR. 

Deployment instructions
=======================

    Prerequisites
    =============
    1. Enable AccountContactRelations in your org.
	-From Setup, go to Account Settings, and select "Allow users to relate a contact to multiple accounts"
        -See https://help.salesforce.com/articleView?id=shared_contacts_set_up.htm&type=5 for more info

    2. Wait 30m for the feature to be loaded properly.
    3. Make sure PSBaseExtension is deployed on the org. If it isn't, then deploy. 

    Deployment
    ==========
    1. Deploy package using ant. 
        - modify the build.properties file with server info.
        - cd to the ant directory. 
        - run the following: ant deployCode
    2. Upsert AFRs using the files in the DataImport directory.
    3. Append {Resource.CustomStylesCubes}/assets/css/customCube.css;{Resource.AccountContactRelationCube}/assets/css/AccountContactRelationCubeStyle.css to ACE.ALM.Application.ExternalResources.StyleSheets
    4. Append {Resource.AccountContactRelationCube}/AccountContactRelationCubeGridEditors.js;{Resource.AccountContactRelationCube}/AccountContactRelationCubeLayout.js to  ACE.ALM.Application.ExternalResources.Scripts
    5. Read docs for more info. 
