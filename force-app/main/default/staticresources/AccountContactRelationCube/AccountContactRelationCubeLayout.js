//=============================================================================
/**
 * Name: AccountContactRelationCubeLayout.js
 * Description: Cube to show secondary account contact relation data. 
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//=============================================================================

(function($)  
{
    function TextEditor(args) 
    {
        var $input;
        var defaultValue;
        var scope = this;
        var writeAccess; 

        this.init = function () 
        {
            // if a row is new, it's Id might be undefined - in that case we can enable it. 
            if (args.item.Contact == null || args.item.Contact.Id == null || args.item.Id.startsWith('new_'))
            {
                writeAccess = true; 
            }
            else 
            {
                writeAccess = Custom.AccountContactRelationCube.prototype.hasWriteAccess(args.item.Id);
            }

            if (args.item.IsActive == "false")
            {
                return; 
            }
            if (!writeAccess)
            {
                PS.AccountContactRelationCubeHelpers.displayNoWriteAccessWarning();
                return; 
            }
            $input = $("<INPUT type=text  />")
                .appendTo(args.container)
                .bind("keydown.nav", function (e) 
                {
                    if (e.keyCode === $.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT) 
                    {
                        e.stopImmediatePropagation();
                    }
                })
                .focus()
                .select();
        };

        this.destroy = function () 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            $input.remove();
        };

        this.focus = function () 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            $input.focus();
        };

        this.getValue = function () 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            return $input.val();
        };

        this.setValue = function (val) 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            $input.val(val);
        };

        this.loadValue = function (item) 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            defaultValue = item[args.column.field] || "";
            $input.val(defaultValue);
            $input[0].defaultValue = defaultValue;
            $input.select();
        };

        this.serializeValue = function () 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            return $input.val();
        };

        this.applyValue = function (item, state) 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            item[args.column.field] = state;
            PS.AccountContactRelationCubeHelpers.updateUpdateList(args.item.Id, args.column.field, state);
        };

        this.isValueChanged = function () 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
        };

        this.validate = function () 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            if (args.column.validator) 
            {
                var validationResults = args.column.validator($input.val());
                if (!validationResults.valid) 
                {
                    return validationResults;
                }
            }

            return {
            valid: true,
            msg: null
            };
        };

        this.init();
    }

    function CheckboxEditor(args) 
    {
        var $select;
        var defaultValue;
        var scope = this;
        scope.args = args;
        var writeAccess;

        this.init = function () 
        {
            // if a row is new, it's Id might be undefined - in that case we can enable it. 
            if (args.item.Contact == null || args.item.Contact.Id == null || args.item.Id.startsWith('new_'))
            {
                writeAccess = true; 
            }
            else 
            {
                writeAccess = Custom.AccountContactRelationCube.prototype.hasWriteAccess(args.item.Id);
            }
            // if (args.item.IsActive == "false")
            // {
            //     return; 
            // } 
            // else if (!writeAccess)
            if (!writeAccess)
            {
                PS.AccountContactRelationCubeHelpers.displayNoWriteAccessWarning();
                return; 
            }

            // $select = $("<INPUT type=checkbox value='true' class='editor-checkbox' hideFocus>");
            var field = args.column.field;
            if (args.item[field] == "true" || args.item[field] == true)
            {
                $select = $("<div class='cubeComponentContainer'><label class='control checkbox'><input type='checkbox'><span class='control-indicator' style='position: relative;''></span></label></div>");
            }
            else 
            {
                $select = $("<div class='cubeComponentContainer'><label class='control checkbox'><input type='checkbox' checked><span class='control-indicator' style='position: relative;''></span></label></div>");
            }

            // first click during init should update. 
            PS.AccountContactRelationCubeHelpers.updateUpdateList(args.item.Id, args.column.field, $select[0].childNodes[0].childNodes[0].checked);

            $select.on("click", handleCheckboxClick);
            $select.appendTo(args.container);
            $select.focus();
        };

        this.destroy = function () 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            } 
            $select.remove();
        };

        this.focus = function () 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            } 
            $select.focus();
        };

        this.loadValue = function (item) 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            } 
        };

        this.serializeValue = function () 
        {
            // if (args.item.IsActive == "false" || !writeAccess)
            if (args.column.field != "IsActive" && (args.item.IsActive == "false" || !writeAccess))
            {
                return; 
            } 
            return $select[0].childNodes[0].childNodes[0].checked;
        };

        this.applyValue = function (item, state) 
        {
            if (!writeAccess)
            {
                return; 
            }

            // when reactivating a active flag... the end date should be cleared out onSave - so don't change the state on the grid but on the updateList...
            // if the user changse the enddate again, updateUpdatelist will be updated again then. 
            if (args.column.field == "IsActive" && !state)
            {
                var todayDate = new Date();
                var miliTime = todayDate.getTime()  + (todayDate.getTimezoneOffset() * 60000);
                item.EndDate = formatDate(todayDate); 
                PS.AccountContactRelationCubeHelpers.updateUpdateList(args.item.Id, "EndDate", formatDate(todayDate));
            }
            else
            {
                item.EndDate = null; 
                PS.AccountContactRelationCubeHelpers.updateUpdateList(args.item.Id, "EndDate", null);
            }

            item[args.column.field] = state;
            // update the updatelist.  
            PS.AccountContactRelationCubeHelpers.updateUpdateList(args.item.Id, args.column.field, state);
        };

        this.isValueChanged = function () 
        {
            if (args.column.field != "IsActive" && (args.item.IsActive == "false" || !writeAccess))
            {
                return; 
            }
            return (this.serializeValue() !== defaultValue);
        };

        this.validate = function () 
        {
            return {
            valid: true,
            msg: null
            };
        };

        function handleCheckboxClick() 
        {
            PS.AccountContactRelationCubeHelpers.updateUpdateList(args.item.Id, args.column.field, $select[0].childNodes[0].childNodes[0].checked);
        }

        this.init();
    }

    function AutoCompleteEditor(args) 
    {
        var myInput,_this;
        this.args = args;

        this.init = function () 
        {
            if (args.column.field == 'Contact.Name') 
            {
                modeValue = "CONTACTS";
            } 
            else if (args.column.field == 'Account.Name') 
            {
                modeValue = "ACCOUNTS";
            } 
            // handle case where mode exists. 

            if (!args.item.id.startsWith("new_")) 
            {
                myInput = $p('<div />');
                myInput.appendTo(args.container);
                return; 
            }

            myInput = $p('<la-input class="required" />');
            myInput.appendTo(args.container);

            // prevents the slickgrid's arrow key from cancelling the search auto complete's arrow key functionality. 
            $(myInput[0]).keydown(function(e){
                if (e.keyCode === $.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT || e.keyCode === $.ui.keyCode.UP || e.keyCode === $.ui.keyCode.DOWN) {
                e.stopPropagation();
                }
            });

            setTimeout(function()
            {
                var columns = args.grid.getColumns();
                var additionalQueryFields = [];

                // loop through the columns, and if there is stuff that should be added to the additionaQueryFields, add it 
                if (columns.length > 0)
                {
                    if (args.column.currentRecordType == 'Account')
                    {
                        for (var i = 0; i < columns.length; i++)
                        {
                            if (columns[i].field.startsWith("Contact"))
                            {
                                if (columns[i].field.split(".")[1] == "Account")
                                {
                                    additionalQueryFields.push("AccountID");
                                }
                                else 
                                {
                                    additionalQueryFields.push(columns[i].field.split(".")[1]);
                                }
                            }
                        }
                    }
                    else 
                    {
                        for (var i = 0; i < columns.length; i++)
                        {
                            if (columns[i].field.startsWith("Account"))
                            {
                                additionalQueryFields.push(columns[i].field.split(".")[1]);
                            }
                        }
                    }
                }

                var autoComplete = new SearchAutoComplete($(myInput[0]),
                {       
                    mode: modeValue,
                    config: args.column.additionalConfig,
                    coverageOnly: false,
                    additionalQueryFields: additionalQueryFields,
                    select: function(value)
                    {
                        if (args.column.currentRecordType == 'Contact')
                        {
                            if (value.Id == args.column.dataProviderCache.AccountId)
                            {
                                displayPrimaryRelationAddedErrorMessage(args.column.currentRecordType, args.column.primaryAccountSelectedErrorMessage);
                                return; 
                            }
                        }

                        if (args.column.currentRecordType == 'Account')
                        {
                            if (value.AccountId == args.column.dataProviderCache.Id)
                            {
                                displayPrimaryRelationAddedErrorMessage(args.column.currentRecordType, args.column.primaryContactSelectedErrorMessage);
                                return; 
                            }
                        }

                        _this.applyValue(value);
                    }   
                });
                $(myInput[0]).on("change", function()
                {
                    _this.applyValue('');
                });
            },5);

        };

        this.loadValue = function (item) 
        {
            // if it's not new - just load the value as is. 
            if (!args.item.id.startsWith("new_")) 
            {
                if (args.column.field == 'Contact.Name') 
                {
                    $(myInput).append(item['Contact']['Name']);
                } 
                else if (args.column.field == 'Account.Name') 
                {
                    $(myInput).append(item['Account']['Name']);
                }
                return; 
            }

            if (args.item.id.startsWith("new_")) 
            {
                if(item[args.column.field] != null)
                {
                    $(myInput[0]).val(item[args.column.field]);
                }
                else if(item[args.column.field.substring(0,args.column.field.indexOf("."))] != null)
                {
                    $(myInput[0]).val(item[args.column.field.substring(0,args.column.field.indexOf("."))].Name);
                }
                else
                {
                    $(myInput[0]).val = "";
                }
            }

            this.item = item;
            _this = this;

        };

        this.serializeValue = function () 
        {
        };

        this.isValueChanged = function () 
        {
        };

        this.destroy = function () 
        {
            if (!args.item.id.startsWith("new_")) 
            {
                return; 
            }
            $(myInput[0]).remove();
        };

        this.applyValue = function (value) 
        {
            //updates the datagrid view with the selected name
            _this.item[_this.args.column.field] = value.Name;

            // dont need to update anyting other than the ID in the update list because the contact info or account info is readonly. 
            if (args.column.field == 'Contact.Name')
            {
                PS.AccountContactRelationCubeHelpers.updateUpdateList(_this.args.item.Id, "ContactId", value.Id);
            } 
            else if (args.column.field == 'Account.Name')
            {
                PS.AccountContactRelationCubeHelpers.updateUpdateList(_this.args.item.Id, "AccountId", value.Id);
            } 
            else 
            {
                PS.AccountContactRelationCubeHelpers.updateUpdateList(_this.args.item.Id, args.column.field, value.Id);
            }      

            

            //update the fields in "item" so that we can save the ids to salesforce 
            var field = _this.args.column.field;
            _this.args.item[field.substring(0,field.indexOf("."))] = value;
            field = field.substring(0,field.indexOf("_")).concat("__c");
            _this.args.item[field] = value.Id;
            _this.args.commitChanges();

            // redraw the rest of the row because data may have changed. 
            args.grid.invalidateAllRows();
            args.grid.render();
        };

        this.validate = function() 
        {
        };

        this.init();
    }

    function PickListEditor(args) 
    {
        var $select;
        var defaultValue = args.item[args.column.field];
        var _this = this;

        this.init = function() 
        {
            opt_values = args.column.picklistOptions;
            option_str = ""
            for(var i = 0; i < opt_values.length; i++)
            {
                v = opt_values[i];
                option_str += "<OPTION value='"+v+"'>"+v+"</OPTION>";
            }
            $select = $("<SELECT tabIndex='0' style='width:100%;' class='editor-select'>"+ option_str +"</SELECT>");
            $select.appendTo(args.container);
            $select.focus();
        };

        this.destroy = function() 
        {
            $select.remove();
        };

        this.focus = function() 
        {
            $select.focus();
        };

        this.loadValue = function(item) 
        {
            defaultValue = item[args.column.field];
            $select.val(defaultValue);
        };

        this.serializeValue = function() 
        {
            if(args.column.picklistOptions)
            {
                return $select.val();
            }
        };

        this.applyValue = function(item,state) 
        {
            item[args.column.field] = state;
            PS.AccountContactRelationCubeHelpers.updateUpdateList(args.item.Id, args.column.field, state);
        };

        this.isValueChanged = function() 
        {
            return ($select.val() != defaultValue);
        };

        this.validate = function() 
        {
            return {
                valid: true,
                msg: null
            };
        };

        this.init();
    }

    function MultiPickListEditor(args) 
    {
        var $select;
        var modal;
        var multiSelectContainer;
        var modalBottomContainer;
        var thisMultiSelect;
        var initialSelectedList;
        var dataItem;
        var _this = this;

        this.init = function() 
        {
            // if a row is new, it's Id might be undefined - in that case we can enable it. 
            if (args.item.Contact == null || args.item.Contact.Id == null || args.item.Id.startsWith('new_'))
            {
                writeAccess = true; 
            }
            else 
            {
                writeAccess = Custom.AccountContactRelationCube.prototype.hasWriteAccess(args.item.Id);
            }
            if (!writeAccess)
            {
                //display warning
                PS.AccountContactRelationCubeHelpers.displayNoWriteAccessWarning();
                return; 
            }
            if (args.item.IsActive == "false")
            {
                return; 
            }

            modal = args.column.modal;
            modal.show();

            // modal header
            try {
                if (args.column.currentRecordType == 'Account')
                {
                    modalHeaderName = args.item.Contact.Name;
                }
                else
                {
                    modalHeaderName = args.item.Account.Name;
                }
            }
            catch(err)
            {
              modalHeaderName = "";
            }
            
            var modalHeader = $('<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix ui-draggable-handle"><span id="ui-id-31" class="ui-dialog-title">Role(s) - ' 
              + modalHeaderName 
              + '</span><button id="acrcModalCloseButton" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" title="Close"><span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text">Close</span></button></div>').appendTo(modal);
            $("#acrcModalCloseButton").on('click', this.destroy);

            // multiselect container
            multiSelectContainer = $('<div class="multiSelectContainer ui-dialog-content ui-widget-content"/>');
            multiSelectContainer.appendTo(modal);
            modalBottomContainer = $('<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"/>');
            modalBottomContainer.appendTo(modal);
        };

        this.destroy = function() 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            $('#accountContactRelationCubeModal').empty();
            modal.hide();
        };

        this.focus = function() 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            modal.focus();
        };

        this.loadValue = function(item) 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            dataItem = item;
            value = item[args.column.field];
            if (value == null)
            {
                initialSelectedList = "";
            }
            else {
                initialSelectedList = value;
            }

            // populate selectedItems from slickgrid item. 
            valueArray = [];
            if (value != "null" && value != undefined)
            {
                valueSplit = value.split(";");
                for (role in valueSplit)
                {
                    if (value.hasOwnProperty(role))
                    {
                        if (valueSplit[role].length > 0)
                        {
                            valueArray.push(valueSplit[role]);
                        }
                    }
                }
            }

            // make the ACEMultiSelect object. 
            var picklistValues = args.column.picklistOptions;
            thisMultiSelect = $(new ACEMultiSelect({dataProvider : picklistValues, selectedItems : valueArray}));
            thisMultiSelect[0].appendTo(multiSelectContainer);

            // ok and cancel buttons 
            // note - hardcoded margins from '.ui-dialog .ui-dialog-buttonpane button' in common.css because the underlying css was not consistent. 
            var cancelButton = $('<button type="button" id="acrcModalCancelButton" class="cancelButton cancelButton ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only la-ripple-container" style="float: right; margin:.5em .4em .5em 0!important;" role="button" ripple="true"><span class="ui-button-text">Cancel</span><la-ripple><span id="la-ripple-ink" class="" style="width: 74px; height: 74px; top: -19.35px; left: 15.825px;"></span></la-ripple></button>').appendTo(modalBottomContainer);     
            var okButton = $('<button type="button" id="acrcModalOkButton" class="acrcModalOkButton confirmButton ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only la-ripple-container" style="float: right;margin:.5em .4em .5em 0!important;"><span class="ui-button-text">OK</span></button>').appendTo(modalBottomContainer);
            $("#acrcModalOkButton").on('click', this.handleOkButton);
            $("#acrcModalCancelButton").on('click', this.handleCancelButton);
        };

        this.serializeValue = function() 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            if (thisMultiSelect[0]._selectedItemsList.options.selectedItems != undefined && thisMultiSelect[0]._selectedItemsList.options.selectedItems != null)
            {
                return thisMultiSelect[0]._selectedItemsList.options.selectedItems;
            } 
        };

        this.applyValue = function(item,state) 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            // convert the array of picklist options to a string.
            var valueString = "";
            for (i in state)
            {
                if (state.hasOwnProperty(i))
                {
                    valueString += state[i] + ';';
                }
            }
            // remove the last semicolon.
            if (valueString.charAt(valueString.length - 1) == ';')
            {
                valueString = valueString.substring(0, valueString.length - 1);
            }

            item[args.column.field] = valueString;
            PS.AccountContactRelationCubeHelpers.updateUpdateList(args.item.Id, args.column.field, valueString);
        };

        this.isValueChanged = function() 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            initialSelectedListSplit = initialSelectedList.split(";");
            // case: 'something;' - should be ['something'] not ['something', '']
            for (i in initialSelectedListSplit)
            {
                if (initialSelectedListSplit[i].length <= 0)
                {
                    initialSelectedListSplit.splice(i, 1);
                }
            }

            console.log(JSON.stringify(initialSelectedListSplit) == JSON.stringify(thisMultiSelect[0]._selectedItemsList.options.selectedItems));

            return JSON.stringify(initialSelectedListSplit) == JSON.stringify(thisMultiSelect[0]._selectedItemsList.options.selectedItems);
        };

        this.validate = function() 
        {
            return {
                valid: true,
                msg: null
            };
        };

        this.handleOkButton = function() 
        {
            _this.applyValue(dataItem, thisMultiSelect[0]._selectedItemsList.options.selectedItems);
            _this.destroy();
            PS.AccountContactRelationCubeHelpers.commitACRCGridChanges();
            return;
        }

        this.handleCancelButton = function() 
        {
            _this.destroy();
            PS.AccountContactRelationCubeHelpers.commitACRCGridChanges();
            return;
        }

        this.init();
    }

    function DateEditor(args) 
    {
        var $input;
        var defaultValue;
        var scope = this;
        var calendarOpen = false;
        var writeAccess;
        // var datePicked = false;
        var _args = args; 
        var dataItem;
        var _this = this; 

        this.init = function () 
        {
            // if a row is new, it's Id might be undefined - in that case we can enable it. 
            if (args.item.Contact == null || args.item.Contact.Id == null || args.item.Id.startsWith('new_'))
            {
                writeAccess = true; 
            }
            else 
            {
                writeAccess = Custom.AccountContactRelationCube.prototype.hasWriteAccess(args.item.Id);
            }

            if (!writeAccess)
            {
                columnField = args.column.field;
                textToDisplay = args.item[columnField];
                $p('<div>' + textToDisplay + '<div/>').appendTo(args.container);
                PS.AccountContactRelationCubeHelpers.displayNoWriteAccessWarning();
                return; 
            }

            var datePickerDiv = $p('<div/>');
            var datePickerSpan = $p('<span/>').appendTo(datePickerDiv);

            var aceDateFormat = ACE.Locale.getDateFormat(ACE.Salesforce.userLocale);

            $input = $("<INPUT type=text class text='editor-text' style='position: relative; z-index: 10000' readonly/>")
            .datepicker(
                {
                    dateFormat: aceDateFormat,
                    onSelect: function(e){
                        if (_this.isValueChanged())
                        {
                            _this.applyValue(_this.dataItem, e);
                            PS.AccountContactRelationCubeHelpers.commitACRCGridChanges();
                        }
                    }
                }
            )
            .bind("change",function(e)
            {
                var d = new Date(e.currentTarget.value);
                var dc = new Date(this.defaultValue);
                if(d.setHours(0,0,0,0) != dc.setHours(0,0,0,0))
                {
                    $(args.column.topDiv).show(333);
                }
                // changing the date updates the updatelist. 
                // PS.AccountContactRelationCubeHelpers.updateUpdateList(args.item.Id, args.column.field, formatDate(new Date($input.val()))); // we update updatelist on applyvalue
            })
            .appendTo(datePickerSpan);

            datePickerDiv.appendTo(args.container);

            $input.datepicker( "option", "dateFormat", ACE.Locale.getDateFormat(ACE.Salesforce.userLocale).toLowerCase().replace("yyyy", "yy"));

            $input.datepicker({
                beforeShow: function () 
                {
                    calendarOpen = true
                },
                onClose: function () 
                {
                    calendarOpen = false
                }
            });
        };

        this.destroy = function () 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            $.datepicker.dpDiv.stop(true, true);
            $input.datepicker("hide");
            $input.removeClass("hasDatepicker");
            $input.datepicker("destroy");
            $input.remove();
        };

        this.show = function () 
        {
            if (calendarOpen) 
            {
                $.datepicker.dpDiv.stop(true, true).show();
            }
        };

        this.hide = function () 
        {
            if (calendarOpen) 
            {
                $.datepicker.dpDiv.stop(true, true).hide();
            }
        };

        this.position = function (position) 
        {
            if (!calendarOpen) 
            {
                return;
            }
        };

        this.focus = function () 
        {
            $input.focus();
        };

        this.loadValue = function (item) 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            _this.dataItem = item; 

            if(item[args.column.field] != "" && item[args.column.field] != null)
            {
                defaultValue = new Date(item[args.column.field]);;
                var dateValue = new Date(defaultValue.getTime() + (defaultValue.getTimezoneOffset() * 60000));
                
                var formattedDate = ACE.DateUtil.format(dateValue);
                $input.val(formattedDate); 

                var aceDateFormat = ACE.Locale.getDateFormat(ACE.Salesforce.userLocale);
                $input[0].defaultValue = dateValue.getTime();
            }
            $input.select();
        };

        this.serializeValue = function () 
        {
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }
            return $input.datepicker('getDate');
        };

        this.applyValue = function (item, state) 
        {
            var date = new Date(state);
            var miliTime = date.getTime()  + (date.getTimezoneOffset() * 60000);

            // deactivate the row if the selected date is earlier than current date
            var todayDate = new Date();
            if (args.column.field == "EndDate" && state != null && todayDate > state)
            {
                item.IsActive = false; 
                PS.AccountContactRelationCubeHelpers.updateUpdateList(args.item.Id, 'IsActive', false);
            }

            // reactivate the row if the enddate is later than the current date. 
            if (args.column.field == "EndDate" && state != null && todayDate < state)
            {
                item.IsActive = true; 
                PS.AccountContactRelationCubeHelpers.updateUpdateList(args.item.Id, 'IsActive', true);
            }
            
            // reactivate the row if enddate is cleared - this technically shouldn't even be possible because if a row is inactive, the row is not editable.
            if (args.column.field == "EndDate" && state == null)
            {
                item.IsActive = true; 
                PS.AccountContactRelationCubeHelpers.updateUpdateList(args.item.Id, 'IsActive', true);
                PS.AccountContactRelationCubeHelpers.updateUpdateList(args.item.Id, args.column.field, null);
                item[args.column.field] = null;
            }
            else
            {
                item[args.column.field] = miliTime;
                PS.AccountContactRelationCubeHelpers.updateUpdateList(args.item.Id, args.column.field, formatDate(date));
            }
            
        };

        this.isValueChanged = function () 
        {
            var dateValue = (defaultValue == null ? null : new Date(defaultValue.getTime() + (defaultValue.getTimezoneOffset() * 60000)));
            var formattedDate = (dateValue == null ? null : ACE.DateUtil.format(dateValue));
            
            if (args.item.IsActive == "false" || !writeAccess)
            {
                return; 
            }

            return $input.val() != formattedDate;
        };

        this.validate = function () 
        {
            return {
                valid: true,
                msg: null
            };
        };

        this.init();
    }

    /**
     * contains email button that read's the rows 'email' column. 
     * AFR - 
     */
    function EmailEditor(args) 
    {
        var emailIcon;
        var writeAccess; 

        this.init = function () 
        {
            // if a row is new, it's Id might be undefined - in that case we can enable it. 
            if (args.item.Contact == null || args.item.Contact.Id == null || args.item.Id.startsWith('new_'))
            {
                writeAccess = true; 
            }
            else 
            {
                writeAccess = Custom.AccountContactRelationCube.prototype.hasWriteAccess(args.item.Id);
            }

            if (args.item.IsActive == "false")
            {
                return; 
            }
            if (!writeAccess)
            {
                PS.AccountContactRelationCubeHelpers.displayNoWriteAccessWarning();
                return; 
            }

            emailIcon = $("<div class=\"accountContactRelationCubeGridEmailButtonCell\"></div>").appendTo(args.container);
            // emailIcon = $("<div>clickme</div>").appendTo(args.container);
            emailFieldName = args.column.field; 

            if (args.item.Contact.HasOptedOutOfEmail == "false") 
            {
                if (args.item[emailFieldName] != undefined && args.item[emailFieldName] != null && args.item[emailFieldName].length > 0) 
                {
                    window.location.href = "mailto:" + args.item[emailFieldName];

                    args.container.click(function() 
                    {
                        window.location.href = "mailto:" + args.item[emailFieldName];
                    });
                }
            }
        };

        this.destroy = function () 
        {
        };

        this.focus = function () 
        {
        };

        this.getValue = function () 
        {
        };

        this.setValue = function (val) 
        {
        };

        this.loadValue = function (item) 
        {
        };

        this.serializeValue = function () 
        {
        };

        this.applyValue = function (item, state) 
        {
        };

        this.isValueChanged = function () 
        {
        };

        this.validate = function () 
        {
        };

        this.init();
    }

    /**
	 * Format js date to yyyy-mm-dd
	 */
    function formatDate(date) 
    {
        var d = new Date(date);
        var month = '' + (d.getMonth() + 1);
        var day = '' + d.getDate();
        var year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    function displayPrimaryRelationAddedErrorMessage(type, errorMessage)
    {
        // if (type == 'Contact')
        // {
        //     errorMessage = "Primary Client cannot be added as a Secondary Affiliate, please try adding another client."
        // }
        // else
        // {
        //     errorMessage = "The contact you are trying to add already exists, please try adding another contact."
        // }
        ACEConfirm.show
        (
            errorMessage,
            "Error",
            [ "OK::save-button confirmButton"],
            function(result)
            {
            }
        );  
    }

    $.extend(true, window, {
        "Slick": {
            "AccountContactRelationCube": {
                "AutoComplete": AutoCompleteEditor,
                "CheckboxEditor": CheckboxEditor, 
                "TextEditor" : TextEditor,
                "PickListEditor": PickListEditor,
                "DateEditor": DateEditor,
                "MultiPickListEditor": MultiPickListEditor,
                "FormatDate": formatDate,
                "EmailEditor": EmailEditor
            }
        }
    });


    /**
     * CUBE STARTS HERE
     */

    var cubeBody = $('<div id="accountContactRelationCubeContainer" class="customCubeContainer"/>');
    var dataProviderCache; 
    var config; 
    var modal; 
    var currentLimit; // limit of number of rows that gets returned. increment by 1000 everytime the user wants to load more. 
    var currentNewId;
    var initialSetDataRan = false; 

    var grid; 
    
    var currentCubeHeight = 0; 
    var showInactiveCache;
    var hasExpandedFilterCache;
    var filterTextCache;
    var cubeDataCache;
    var updateListCache; 
    var resultsAllCache; 
    var resultsActiveOnlyCache;
    var shouldShowSaveAndCancelButton;
    var deactivatedContactList = new Set(); // keep track of which contacts got deactivated... if this list is populated, then we show a deactivation message. 
    var originallyActiveACRIdList = new Set();

    // caching the last selected mainPane item because the cube does not run SetData on initialization.
    // we manually run setData on initialization using this data. 
    // ALM.modelLocator.watch('mainPaneSelectedItem', function(selectedItem)
    // {
    //     $.extend(true, window,
    //     {
    //     "Custom":
    //     {
    //         "mainPaneSelectedItemCache": selectedMainPaneItem
    //     }});
    // });	

    function AccountContactRelationCube() 
    {
        // get soap libs from salesforce so we can connect to salesforce. 
        if (window.sforce == null)
        {
            $("head").append('<script type="text/javascript" src="/soap/ajax/42.0/connection.js" />');
            $("head").append('<script type="text/javascript" src="/soap/ajax/42.0/apex.js" />');
        }

        // // intial setData run...
        // if (!initialSetDataRan)
        // {
        //     this.__proto__.setData(Custom.mainPaneSelectedItemCache);
        //     initialSetDataRan = true; 
        // }
    }

    AccountContactRelationCube.prototype.clear = function()
    {
        $(cubeBody).empty();
    }

    AccountContactRelationCube.prototype.cancel = function()
    {
        //Cancel the current pull transaction
        if (this._token != null)
        {
            this._token.cancel();
            this._token = null;
        }
    }

    AccountContactRelationCube.prototype.setData = function(dataProvider)
    { 
        if (this._token != null) 
        {
            this._token.cancel();
            this._token = null;
        }

        this._token = new AsyncToken(resultHandler);

        dataProviderCache = dataProvider;

        // assigns session id and executes webservice. 
        sforce.connection.sessionId = ACE.Salesforce.sessionId;  // Get the Session Id for Authentication

        // get configs and cube data. 
        sforce.apex.execute("AccountContactRelationCubeController", "getConfig", {}, 
        { 
            onSuccess: function(result)
            {
                config = JSON.parse(result);
                currentLimit = 1000;
                hasExpandedFilterCache = config.isOpen;
                showInactiveCache = config.showInactive;
                filterTextCache = '';

                sforce.apex.execute("AccountContactRelationCubeController", "getCubeData", {recordId : dataProvider.Id, loggedInUserId  : ALM.modelLocator.userId(), loadLimit : currentLimit}, 
                { 
                    onSuccess: _token.onSuccess,        
                    onFailure: _token.onFailure
                });
            },    
            onFailure: function(result)
            {
                console.log("Contact your administrator - AFR failure");
                console.log(result);
                var errormsg = "";
                
                if ( result.faultcode.includes("INSUFFICIENT_ACCESS") || result.faultstring.includes("INSUFFICIENT_ACCESS") )
                {
                    errormsg = "You do not have permissions to view this Contact. Please contact your administrator";
                }
                                
                ACE.FaultHandlerDialog.show
                ({
                    title:   "Warning",
                    message: errormsg, 
                    error:   result.faultstring
                });
            }
        });
    }

    AccountContactRelationCube.prototype.measure = function()
    {
    }

    AccountContactRelationCube.prototype.createChildren = function(parent)
    {   
        $(cubeBody).appendTo(parent);
    }

    function AsyncToken(resultHandler)
    {
        this._isCancelled = false;
        this._resultHandler = resultHandler;

        var _this = this;
        _token = _this;

        this.onSuccess = function(result)
        {
            _result = result;
            if (!_this._isCancelled)
            {
            _this._resultHandler(result);
            }
        };

        this.onFailure = function(message)
        {
            console.log(message);
            if (!_this._isCancelled)
            {
            ACE.FaultHandlerDialog.show(message.message);
            }
        };
    }

    AsyncToken.prototype.cancel = function()
    {
        this._isCancelled = true;
        _token._isCancelled = true;
    };

    function resultHandler(result) 
    {
        AccountContactRelationCube.prototype.clear();

        result = result[0]; // unwrap AccountContactRelationCubeData object from array

        // store the results in the container. 
        storeResults(result);
        storeResultsActiveOnly();

        // make all the wrappers
        var collapsibleContainerButtonWrapper 
            = $('<div id="collapsibleContainerButtonWrapper"/>').appendTo(cubeBody);
        var addNewButtonContainer
            = $('<div id="addNewButtonContainer" class="powerCubeButtonContainer" style="padding-bottom: 5px" />').appendTo(cubeBody);
        var indirectAccountsTableSection = $('<div Id="tableContainer"></div>').appendTo(cubeBody);
        updateListCache = {};
        currentNewId = 0;

        // reset flags when you requery. 
        deactivatedContactList = new Set();
        originallyActiveACRIdList = new Set();

        // hardcoded some css math to center the modal
        modal = $('<div id="accountContactRelationCubeModal" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front multiSelectDialog dataGridColumnManager aceDialog ui-dialog-buttons ui-draggable" tabindex="-1" role="dialog" style="height: auto!important; width: 450px!important; min-width:450px!important; top:100px; left:calc((50% - 225px)); display: block; z-index: 1002; bottom: auto;"/>').appendTo(cubeBody); 
        modal.hide();

        // make filter
        addAccountContactRelationGridFilter();

        var saveAndCancelButtonsContainer
            = $('<div id="saveAndCancelButtonsContainer" class="powerCubeButtonContainer" style="padding-top: 8px; position: relative; bottom: 10px;"/>').appendTo(cubeBody);
        shouldShowSaveAndCancelButton = false; 
        addSaveAndCancelButtons();

        // choose which result view to use and make the grid
        if (showInactiveCache) 
        {
            addAccountContactRelationGrid(resultsAllCache);
        }
        else {
            addAccountContactRelationGrid(resultsActiveOnlyCache);
        }

        // hack - jQuery append() doesnt have a callback... so we need to wait a bit for it to go into the dom
        setTimeout(function(){ 
            $("#showInactiveAccountContactRelationsCheckbox").click(function(){
                handleShowInactiveCheckbox();
            }); 
        }, 50);
    }

    function addAccountContactRelationGridFilter() 
    {
        if (hasExpandedFilterCache)
        {
            turnFilterOn();
        }
        else 
        {
            turnFilterOff();
        }

        // buttons
        var addNewButton
            = $('<button type="button" class="applyToSelected la-ripple-container" style="float: right;">' + config.newButtonLabel +'</button>').appendTo($('#addNewButtonContainer'));
        addNewButton.on("click", handleNewButton);
    }

    function turnFilterOn() 
    {
        $("#collapsibleContainerButtonWrapper").empty();
        var collapsibleContainer
            = $('<div class="collapsibleContainer containerOpen"/>').appendTo($('#collapsibleContainerButtonWrapper'));
        var collapseSection
            = $('<la-collapse id="collapsibleSection" style="height: 0px;" open/>').appendTo(collapsibleContainer);
        var collapseButtonWrapper
            = $('<div class="collapsibleContainerButtonWrapper">').appendTo(collapsibleContainer);
        var collapseButton
            = $('<div class="collapsibleContainerButton open" title="" data-has-title="true"></div>').appendTo(collapseButtonWrapper);
        collapseButton.on("click", handleFilterToggle);
        hasExpandedFilterCache = true; 
        addAccountContactRelationGridFilterContent();
        updateFilterButtonState(true);
        setGridHeight(true);
    }

    function turnFilterOff() 
    {
        $("#collapsibleContainerButtonWrapper").empty();
        var collapsibleContainer
            = $('<div class="collapsibleContainer"/>').appendTo($('#collapsibleContainerButtonWrapper'));
        var collapseSection
            = $('<la-collapse id="collapsibleSection" style="height: 0px;" />').appendTo(collapsibleContainer);
        var collapseButtonWrapper
            = $('<div class="collapsibleContainerButtonWrapper">').appendTo(collapsibleContainer);
        var collapseButton
            = $('<div class="collapsibleContainerButton" title="" data-has-title="true"></div>').appendTo(collapseButtonWrapper);
        collapseButton.on("click", handleFilterToggle);
        hasExpandedFilterCache = false; 
        addAccountContactRelationGridFilterContent();
        updateFilterButtonState(false);
        setGridHeight(true);
    }

    /**
	 * Used for sorting by the Order AFR.
	 */
    function compare(a,b) 
    {
        if (a.order < b.order)
        {
            return -1;
        }
        if (a.order > b.order)
        {
            return 1;
        }
        return 0;
    }

    /**
	 * Generates the markup for inside the collapsible filter section.
	 */
    function addAccountContactRelationGridFilterContent()
    {
        // generate inactive checkbox and label
        var showInactiveContainer
            = $('<div id="showInactiveContainer" class="componentContainer subSectionItem" />').appendTo($("#collapsibleSection"));
        var showInactiveCheckboxContainer
            = $('<label class="control checkbox" />').appendTo(showInactiveContainer);

        // generate checkbox given the checkbox state in the wrapper
        if (showInactiveCache) 
        {
            var showInactiveCheckbox
            = $('<input type="checkbox" id="showInactiveAccountContactRelationsCheckbox" class="primaryLabel" checked />').appendTo(showInactiveCheckboxContainer);
        }
        else 
        {
            var showInactiveCheckbox
            = $('<input type="checkbox" id="showInactiveAccountContactRelationsCheckbox" class="primaryLabel" />').appendTo(showInactiveCheckboxContainer);
        }

        $("#showInactiveAccountContactRelationsCheckbox").click(function(){
            handleShowInactiveCheckbox();
        });

        var showInactiveCheckboxSpan
            = $('<span class="control-indicator" style="position: relative;"/>').appendTo(showInactiveCheckboxContainer);
        // var showInactiveCheckboxText
        //     = $('<div id="showInactiveLabel" class="label">' + config.showInactiveLabel + '</div>').appendTo(showInactiveContainer);
        
        var showInactiveCheckboxText
            = $('<div id="showInactiveLabel" class="label" style="padding-left:5px;">' + config.showInactiveLabel + '</div>').appendTo(showInactiveContainer);

        // generate textarea given the state in the wrapper
        var filterTextArea
            = $('<la-input id="acrcFilterTextArea" placeholder="' + config.filterTextPlaceholderLabel + '" />').appendTo($("#collapsibleSection"));
        $('#acrcFilterTextArea').val(filterTextCache);
        $(document).on("keyup", handleFilterTextChange); //changed from filterTextArea.on() to $(document).on() because it doesnt work on IE. 
    }

    function addAccountContactRelationGrid(result) 
    {
        if (result == null)
        {
            return;
        }

        for (var i = 0; i < result.length; i++)
        {
            if (result[i].IsActive == "true" || result[i].IsActive == true)
            {
                originallyActiveACRIdList.add(result[i].Id);
            }
        }

        currentRecordType = cubeDataCache.currentRecordType;

        // make section and header
        var indirectAccountContactRelationTableSection = $('<div Id="IndirectAccountContactRelationTableSection" class="section accountSummary"></div>').appendTo($('#tableContainer'));
        var indirectAccountContactRelationTableHeader = $('<div class="sectionHeader accountHeader"></div>').appendTo(indirectAccountContactRelationTableSection);
        var indirectAccountContactRelationTableHeaderTitleContainer = $('<div class="headerItem headerTitleContainer"></div>').appendTo(indirectAccountContactRelationTableHeader);

        // make grid section and grid
        var indirectAccountContactRelationGridSection = $('<div id="ACRCDataGridContainer" class="subSection cubeDataGridContainer"></div>').appendTo(indirectAccountContactRelationTableHeaderTitleContainer);
        setGridHeight();

        if ($(indirectAccountContactRelationGridSection)[0] != null)
        {
            var indirectAccountContactRelationGrid = new ACEDataGrid($(indirectAccountContactRelationGridSection), { 
                forceFitColumns: false, 
                explicitInitialization: true,
                editable: true,                             
                headerRowHeight: 40,
                sortable: true
            });
        }
        else 
        {
            return; 
        }        

        columns = [];
        config.columnConfigs.sort(compare); // sorts based on ORDER from the afr. 

        // formatters
        var isActiveFormatter = function(row, cell, value, columnDef, dataContext) 
        {
            disabled = ""; 

            // if (dataContext.isActiveCache == false || dataContext.isActiveCache == 'false' )
            // {
            //     disabled = " disabled='disabled' "; 
            // }

            if (value == "true" || value == true) 
            {
                return "<label class='control checkbox'><input type='checkbox' id='isActive-ID-" + dataContext.id 
                + "' class='primaryLabel' " + disabled + "checked><span class='control-indicator' style='position: relative;' /></label>";
            }
            else 
            {
                return "<label class='control checkbox'><input type='checkbox' id='isActive-ID-" + dataContext.id 
                + "' class='primaryLabel'" + disabled + "><span class='control-indicator' style='position: relative;' /></label>";
            }
        }

        var dateFormatter = function(row,cell,value,columnDef,dataContext)
        {  
            if(value === '')
            {
                return "<span class='fakeDatePicker'></span>";
            }
            var dateValue = new Date(value);
            var dateString = '';
            var correctedMilliTime = dateValue.getTime()  + (dateValue.getTimezoneOffset() * 60000);
            if(correctedMilliTime > 0)
            {
                dateValue = new Date(correctedMilliTime);
                dateString = ACE.DateUtil.format(dateValue);
                // dateString = dateValue.toLocaleDateString();
            }
            return "<span class='fakeDatePicker'>" + dateString + "</span>";
        }

        var autoCompleteFormatter = function(row,cell,value,columnDef,dataContext)
        {  
            dataContext.fullName = value; 
            if (value == undefined || value == null || value == "")
            {
                return '<div class="required">&nbsp;</div>';
            }
            else {
                return value;
            }
        }

        var textFormatter = function(row,cell,value,columnDef,dataContext)
        {  
            return value; 
        }

        // multipicklist cells are comma delimited instead of semicolon delimited. 
        var multiPicklistFormatter = function(row,cell,value,columnDef,dataContext)
        {  
            return "<div class=\"role-cell\">" + value.replace(/;/g, ", ") + "</div>";
        }

        var emailButtonFormatter = function(row,cell,value,columnDef,dataContext)
        {
            return '<div class=\"accountContactRelationCubeGridEmailButtonCell\"></div>';
        }

        curColId = 0; 
        columnToOrder = ''; // for setSortColumn. 
        for (columnConfig in config.columnConfigs)
        {
            if (config.columnConfigs[columnConfig].visible == false)
            {
                continue;
            }

            // if current = account, disable accounts and vice versa. 
            if ((currentRecordType == 'Account' && config.columnConfigs[columnConfig].dataField == 'Account.Name')
            || (currentRecordType == 'Contact' && config.columnConfigs[columnConfig].dataField == 'Contact.Name'))
            {
                continue;
            }

            // disable columns that arent enabled for their type. 
            if (config.columnConfigs[columnConfig].enabledTypes != null 
                    && (currentRecordType == 'Account' && config.columnConfigs[columnConfig].enabledTypes.toUpperCase() == 'CONTACT'
                    || currentRecordType == 'Contact' && config.columnConfigs[columnConfig].enabledTypes.toUpperCase() == 'ACCOUNT'))
            {
                continue; 
            }

            if (config.columnConfigs.hasOwnProperty(columnConfig))
            {
                var newColumn = {};

                newColumn.id = "col_" + curColId;
                curColId += 1;
                newColumn.field = config.columnConfigs[columnConfig].dataField;
                newColumn.name = config.columnConfigs[columnConfig].label;
                newColumn.sortable = true; 
                newColumn.currentRecordType = cubeDataCache.currentRecordType;
                newColumn.currentRecordId = cubeDataCache.currentRecordId;
                newColumn.dataProviderCache = dataProviderCache;
                newColumn.width = config.columnConfigs[columnConfig].width;
                newColumn.readOnly = config.columnConfigs[columnConfig].readOnly;

                switch(config.columnConfigs[columnConfig].fieldType)
                {
                    case "EMAILBUTTON":
                        newColumn.formatter = emailButtonFormatter;
                        if (!config.columnConfigs[columnConfig].readOnly)
                        {
                            newColumn.editor = Slick.AccountContactRelationCube.EmailEditor;
                        }
                        break;
                    case "CHECKBOX":
                        if (!config.columnConfigs[columnConfig].readOnly)
                        {
                            newColumn.editor = Slick.AccountContactRelationCube.CheckboxEditor;
                        }   
                        newColumn.formatter = isActiveFormatter;
                        break;
                    case "DATE":
                        if (!config.columnConfigs[columnConfig].readOnly)
                        {
                            newColumn.editor = Slick.AccountContactRelationCube.DateEditor;
                        }
                        newColumn.formatter = dateFormatter;
                        break;
                    case "MULTIPICKLIST":
                        if (!config.columnConfigs[columnConfig].readOnly)
                        {
                            newColumn.picklistOptions = config.columnConfigs[columnConfig].picklistOptions;
                            newColumn.editor = Slick.AccountContactRelationCube.MultiPickListEditor;
                            newColumn.modal = modal;
                        }
                        newColumn.formatter = multiPicklistFormatter;
                        break;
                    case "PICKLIST":
                        if (!config.columnConfigs[columnConfig].readOnly)
                        {
                            newColumn.picklistOptions =  config.columnConfigs[columnConfig].picklistOptions;
                            newColumn.editor = Slick.AccountContactRelationCube.PickListEditor;
                            newColumn.modal = modal;
                        }
                        newColumn.formatter = multiPicklistFormatter;
                        break;
                    case "AUTOCOMPLETE":
                        if (!config.columnConfigs[columnConfig].readOnly)
                        {
                            newColumn.editor = Slick.AccountContactRelationCube.AutoComplete;
                        }
                        newColumn.formatter = autoCompleteFormatter;
                        newColumn.primaryAccountSelectedErrorMessage = config.primaryAccountSelectedErrorMessage;
                        newColumn.primaryContactSelectedErrorMessage = config.primaryContactSelectedErrorMessage;
                        break;
                    case "TEXT":
                        if (!config.columnConfigs[columnConfig].readOnly)
                        {
                            newColumn.editor = Slick.AccountContactRelationCube.TextEditor;
                        }
                        newColumn.formatter = textFormatter;
                        break;
                }
                columns.push(newColumn);

                //set the grid default order.  cubeDataCache.sortDirection
                if ((newColumn.field == 'Account.Name' || newColumn.field == 'Contact.Name') && cubeDataCache.sortField == 'Name')
                {
                    columnToOrder = newColumn.id;
                }
                else if (newColumn.field == cubeDataCache.sortField)
                {
                    columnToOrder = newColumn.id;
                }
            }
        }

        // slickgrid requires an "id" key. 
        tableData = result;

        if (tableData[0] !== undefined) 
        {
            for (tableEntry in tableData)
            {
            if (currentRecordType == 'Account')
            {
                tableData[tableEntry]["id"] = tableData[tableEntry]["Id"];
            }
            else if (currentRecordType == 'Contact') 
            {
                tableData[tableEntry]["id"] = tableData[tableEntry]["AccountId"];
            }
            
            }
        } 
        else 
        {
            // if there is exactly 1 indirect contact, result is a record, not an array of records. 
            if (currentRecordType == 'Account')
            {
            tableData["id"] = tableData["Id"];
            }
            else if (currentRecordType == 'Contact')
            {
            tableData["id"] = tableData["AccountId"];
            }
        }

        // make isActiveCache for each row. this will be a cached version of isActive that is unchangable. used for when determining if a row is editable or not. 
        if (tableData[0] !== undefined) 
        {
            for (tableEntry in tableData)
            {
            tableData[tableEntry]["isActiveCache"] = tableData[tableEntry]["IsActive"];
            }
        } 
        else 
        {
            // if there is exactly 1 indirect contact, result is a record, not an array of records. 
            tableData["isActiveCache"] = tableData["IsActive"];
        }

        indirectAccountContactRelationGrid.setColumns(columns); //, formatter);
        indirectAccountContactRelationGrid.grid.init();
        indirectAccountContactRelationGrid.addItems(tableData);

        grid = indirectAccountContactRelationGrid;

        // make inactive rows uneditable - except for the isActive column. 
        grid.grid.onBeforeEditCell.subscribe(function(e, args)
        {   
            // if (args.item.isActiveCache == false || args.item.isActiveCache == 'false')
            if (args.item.IsActive == false || args.item.IsActive == 'false')
            {
                if (args.column.field == "IsActive" || args.column.field == "EndDate")
                {
                    return true; 
                }
                return false; 
            }
        });

        // make inactive rows uneditable - except for the isActive column. 
        grid.grid.onCellChange.subscribe(function(e, args)
        {   
            var item = args.item;
            item.hasChangedFlag = true; 
        });


        grid.dataProvider.setFilter(
            function(item)
            {
                if (filterTextCache == null)
                {
                    return true; 
                }

                for (var property in item)
                {
                    if (item.hasOwnProperty(property))
                    {
                        if (item[property].toString().toLowerCase().includes(filterTextCache.toLowerCase()) && property != "Id" && property != "ContactId" && property != "AccountId") // don't match on id. 
                        {
                            // special case for contact and account because they include IDs and we don't want to filter for those. 
                            if (property == "Contact" || property == "Account")
                            {
                                for (var innerProperty in item[property])
                                {
                                    if (item[property].hasOwnProperty(innerProperty))
                                    {
                                        // special case... dont match Contact.Account.Id, Account.Id or Contact.Id 
                                        if (property == "Contact" && innerProperty == "Account")
                                        {
                                            if (item["Contact"]["Account"]["Name"].toString().toLowerCase().includes(filterTextCache.toLowerCase()))
                                            {
                                                return true; 
                                            }
                                        }
                                        else 
                                        {
                                            if (innerProperty != "AccountId" && innerProperty != "Id" && item[property][innerProperty].toString().toLowerCase().includes(filterTextCache.toLowerCase()))
                                            {
                                                return true;
                                            }
                                        }
                                    }
                                }
                                return false; 
                            }
                            else // property isnt a contact or account
                            {
                                if (property != "id" && property != "Id")
                                {
                                    if (item[property].toString().toLowerCase().includes(filterTextCache.toLowerCase()))
                                    {
                                        return true; 
                                    }
                                }   
                            }
                        }
                    }
                }
                return false;  
            }
        );

        // default sorting
        sortOrderIsAscending = true;
        if (cubeDataCache.sortDirection.toLowerCase() == 'desc')
        {
            sortOrderIsAscending = false;
        }
        grid.grid.setSortColumn(columnToOrder, sortOrderIsAscending); //columnId, ascending (ascneding = true)
        grid.sort();

        grid.grid.onColumnsReordered.subscribe(function(e,args)
        {
            updateColumnOrder(e, args);
        });

        grid.grid.onColumnsResized.subscribe(function(e,args)
        {
            updateColumnWidth(e, args);
        });

        $(window).on('pointerup', function() 
        {
            handleFilterTextChange();

            // if the borders of the cube are changed... the container size should be different from the cached current cube height. 
            if (getGridContainerHeight() != currentCubeHeight)
            {
                setGridHeight();
            }
        });

        $(window).on('resize', function()
        {
            if (getGridContainerHeight() != currentCubeHeight)
            {
                setGridHeight();
            }
        });

    }

    /**
     * Determine if the current user has write access to a given relation ID.
     */ 
    AccountContactRelationCube.prototype.hasWriteAccess = function(relationId)
    {
        var writableRelations = cubeDataCache.relationsWithWriteAccess;
        // relationsWithWriteAccess is string if there is 1 entry, else an array of strings. 
        if (writableRelations instanceof Array) 
        {
            for (relationIndex in writableRelations)
            {
                if (writableRelations.hasOwnProperty(relationIndex))
                {
                    if (relationId == writableRelations[relationIndex])
                    {
                        return true; 
                    }
                }
            }
        } 
        else if (typeof writableRelations === "string") 
        {
            if (relationId == writableRelations)
            {
                return true; 
            }
        }
        return false; 
    }

    /**
     * When something happens that causes the grid conteiner to change, set the current grid height and redraw it. 
     */
    function setGridHeight(addDelay)
    {
        if (grid != undefined && grid != null)
        {
            if (addDelay)
            {
                // 300ms delay before redrawing because the filter has an animation and we don't want to redraw too early. 
                setTimeout(function()
                {
                    currentCubeHeight = getGridContainerHeight();
                    grid.grid.resizeCanvas();
                }, 300);
            } 
            else 
            {
                currentCubeHeight = getGridContainerHeight();
                grid.grid.resizeCanvas();
            }
            
        }
    }

    /**
	 * Calculates the height available for the grid. 
	 */

    function getGridContainerHeight() 
    {
        return $('#ACRCDataGridContainer').outerHeight();
    }

    function updateColumnOrder(e, args) 
    {
        var settingsToUpdate = args.grid.getColumns();
        for(var i = 0; i < settingsToUpdate.length; i++)
        {
            var setting = settingsToUpdate[i];
            setting.order = i;
        }	

        columnOrder = {}; 

        // double for loop (columns and configcolumns) to get a map of path:order.  
        for (columnSetting in settingsToUpdate)
        {
            if (settingsToUpdate.hasOwnProperty(columnSetting))
            {
                // loop through available attributes.
                for (columnConfig in config.columnConfigs)
                {
                    if (config.columnConfigs.hasOwnProperty(columnConfig))
                    {
                        // see if they match.
                        if (settingsToUpdate[columnSetting].field == config.columnConfigs[columnConfig]["dataField"])
                        {
                            columnOrder[config.columnConfigs[columnConfig]["featurePath"]] = (parseInt(columnSetting, 10) + 1) * 5;
                        }
                    }
                }
            }
        }

        var result = sforce.apex.execute("AccountContactRelationCubeController", "reorderColumns", {userId : ALM.modelLocator.userId(), attributeMap : JSON.stringify(columnOrder)}, 
            { 
                onSuccess: function(result){}, 
                onFailure: function(message)
                {			
                    console.log(message);
                    ACE.FaultHandlerDialog.show(message);
                } 
        });   
    }

    /**
     * Saves user width preferences. 
     */
    function updateColumnWidth(e, args) 
    {
        var columnData = args.grid.getColumns();
        var columnWidth = {};

        // double for loop (columns and configcolumns) to get a map of path:width. 
        for (columnSetting in columnData)
        {
            if (columnData.hasOwnProperty(columnSetting))
            {
                // loop through available attributes.
                for (columnConfig in config.columnConfigs)
                {
                    if (config.columnConfigs.hasOwnProperty(columnConfig))
                    {
                        // see if they match.
                        if (columnData[columnSetting].field == config.columnConfigs[columnConfig]["dataField"])
                        {
                            columnWidth[config.columnConfigs[columnConfig]["featurePath"]] = columnData[columnSetting].width;
                        }
                    }
                }
            }
        }

        var result = sforce.apex.execute("AccountContactRelationCubeController", "updateColumnWidth", {userId : ALM.modelLocator.userId(), featureToWidth : JSON.stringify(columnWidth)}, 
        { 
            onSuccess: function(result){}, 
            onFailure: function(message)
            {			
                console.log(message);
                ACE.FaultHandlerDialog.show(message);
            } 
        });   
    }

    function updateFilterButtonState(isOpen)
    {
        var result = sforce.apex.execute("AccountContactRelationCubeController", "updateFilterButtonState", {userId : ALM.modelLocator.userId(), isOpen : JSON.stringify(isOpen)}, 
        { 
            onSuccess: function(result){}, 
            onFailure: function(message){} 
        });
    }

    function addSaveAndCancelButtons() 
    {
        container = $("#saveAndCancelButtonsContainer");
        container.empty();

        if (shouldShowSaveAndCancelButton)
        {
            var cancelButton
            = $('<button type="button" id="cancelButton" class="applyToSelected la-ripple-container" style="float: right;">' + config.cancelButtonLabel + '</button>').appendTo(container);
            var saveButton
            = $('<button type="button" id="saveButton" class="applyToSelected la-ripple-container" style="float: right;">' + config.saveButtonLabel + '</button>').appendTo(container);
            cancelButton.on("click", handleCancelButton);
            saveButton.on("click", handleSaveButton);
        }
        var loadNext1000Button
        = $('<button type="button" id="loadNext1000Button" class="applyToSelected la-ripple-container" style="float: left;">' + config.loadButtonLabel + '</button>').appendTo(container);
        loadNext1000Button.on("click", handleLoadNext1000Button);
    }

    /**
	 * Hide or show the filter content when chevron is clicked. 
	 */
    function handleFilterToggle()
    {
        if (hasExpandedFilterCache)
        {
            turnFilterOff();
        } 
        else 
        {
            turnFilterOn();
        }
    }

    function handleShowInactiveCheckbox() 
    {
        // if it's currently editin a cell, commit it.
        grid.grid.getEditorLock().commitCurrentEdit();

        $("#tableContainer").empty();
        if ($('#showInactiveAccountContactRelationsCheckbox').is(":checked"))
        {
            // update state in wrapper so minimizing the filter won't affect it. 
            showInactiveCache = true; 
            addAccountContactRelationGrid(resultsAllCache);
        }
        else 
        {
            showInactiveCache = false; 
            storeResultsActiveOnly();
            addAccountContactRelationGrid(resultsActiveOnlyCache);
        }
    }

    /**
     * Updates the state of the text in the collapsible filter wrapper. 
     */
    function handleFilterTextChange()
    {
        // timeout to wait until the x button deleted the stuff from the text input. 
        setTimeout(function() 
        {
            if (filterTextCache != $('#acrcFilterTextArea').val())
            {
                filterTextCache = $('#acrcFilterTextArea').val();
                grid.dataProvider.refresh();
            }
        }, 50);
    }

    /**
     * Caches the data given from the query. 
     */
    function storeResults(results) 
    {
        cubeDataCache = results;
        if (cubeDataCache.currentRecordType == 'Account') 
        {
            resultsAllCache = results.indirectContacts;
        } 
        else if (cubeDataCache.currentRecordType == 'Contact') 
        {
            resultsAllCache = results.indirectAccounts;
        } 
        if (resultsAllCache == null) 
        {
            resultsAllCache = [];
        }
    }

    /**
	 * Get subset of rows that are active and cache it to resultsActiveOnly. 
	 */
    function storeResultsActiveOnly()
    {
        var resultsActiveOnly = [];
        results = resultsAllCache;

        // case where if size of data is 1, results isn't an array. 
        if (results[0] == undefined) 
        {
            if (results['IsActive'] == "true" || results['IsActive'] == true) 
            {
                resultsActiveOnly.push(results);
            }
        }
        else
        {
            for (i in results)
            {
                if (results[i]['IsActive'] == "true" || results[i]['IsActive'] == true) 
                {
                    resultsActiveOnly.push(results[i]);
                }
            }
        }
        resultsActiveOnlyCache = resultsActiveOnly;
    }

    /**
	 * Add a new blank row to the grid. 
	 */
    function handleNewButton() 
    {
        // if it's currently editin a cell, commit it. 
        grid.grid.getEditorLock().commitCurrentEdit();

        newId = "new_" + currentNewId;

        // set startdate to todays date 
        var today = new Date();
        today.setHours(0, 0, 0, 0);

        // make the new record
        var newItem = { 
            "id": newId, 
            "Id": newId, 
            "IsActive": 'true', 
            "StartDate": Slick.AccountContactRelationCube.FormatDate(today),
            "hasChangedFlag": false // for new records, flag to see if this has been changed at all. 
        };       
        updateUpdateList(newId, "StartDate", Slick.AccountContactRelationCube.FormatDate(today), true);
        currentNewId++; 
        grid.addItems(newItem);

        // Add the new relation to the dataset. 
        resultsAllCache.push(newItem);

        shouldShowSaveAndCancelButton = true; 
        addSaveAndCancelButtons();
    }

    /**
     * Given a acr ID, turn it inactive. 
     * @param {*} accountContactRelationId 
     */
    function setInactive(accountContactRelationId)
    {
        var gridItems = grid.getItems();

        for (var itemIndex in gridItems) 
        {
            if (gridItems.hasOwnProperty(itemIndex)) 
            {
                var item = gridItems[itemIndex];
                if (item.Id == accountContactRelationId)
                {
                    item.IsActive = false; 
                }
            }
        }

        grid.setItems(gridItems);
        commitACRCGridChanges();
    }

    /**
	 * Updates the Update List when content in the slickgrid changes (i.e. user updates a account/contact relation)
	 */
    function updateUpdateList(accountContactRelationId, fieldToReplace, newValue, isNew) 
    {
        
        if (fieldToReplace == 'IsActive' && newValue == false)
        {
            deactivatedContactList.add(accountContactRelationId);
        }
        else if (fieldToReplace == 'IsActive' && newValue == true)
        {
            deactivatedContactList.delete(accountContactRelationId);
        }

        // if enddate is set to today, then we set active to false. 
        var today = new Date();
        today.setHours(0, 0, 0, 0);
        if (fieldToReplace == 'EndDate' && newValue == Slick.AccountContactRelationCube.FormatDate(today))
        {
            updateUpdateList(accountContactRelationId, 'IsActive', false);
            setInactive(accountContactRelationId);
            // shouldShowDeactivationConfirmation = true;
        }

        

        if (accountContactRelationId in updateListCache) 
        {
            updateListCache[accountContactRelationId][fieldToReplace] = newValue; 

            // update the changed flag since the updated row is no longer new. 
            if (isNew == null || (isNew != null && !isNew))
            {
                updateListCache[accountContactRelationId]['hasChangedFlag'] = true;
            }
        }
        else 
        {
            var o = {};
            o[fieldToReplace] = newValue;
            if (!accountContactRelationId.startsWith('new_'))
            {
                o.hasChangedFlag = true; 
            }
            updateListCache[accountContactRelationId] = o;
        }

        console.log(updateListCache);

        shouldShowSaveAndCancelButton = true; 
        addSaveAndCancelButtons();
    }

    function handleSave() 
    {
        // if it's currently editin a cell, commit it. 
        grid.grid.getEditorLock().commitCurrentEdit();

        // don't save if there's dupes.  
        var hasDupes = handleDupes();
        if (hasDupes)
        {
            return; 
        }
        
        // make a copy of updatelist cache without the items without hasChangedFlag
        var updateListToCommit = {}; 
        for (var key in updateListCache) {
            if (updateListCache.hasOwnProperty(key)) {
                console.log(updateListCache[key]);
                if (updateListCache[key].hasChangedFlag != null && updateListCache[key].hasChangedFlag)
                {
                    updateListToCommit[key] = JSON.parse(JSON.stringify(updateListCache[key]));
                     delete updateListToCommit[key]['hasChangedFlag'];
                }
            }
        }

        sforce.apex.execute("AccountContactRelationCubeController", "updateAccountContactRelations", 
                { 
                currentRecordId : JSON.stringify(cubeDataCache.currentRecordId), 
                currentRecordType : JSON.stringify(cubeDataCache.currentRecordType),
                // updateList : JSON.stringify(updateListCache)
                updateList : JSON.stringify(updateListToCommit)
                },
            { 
            onSuccess: function(result)
            {
                handleFriendlyErrorMessage(result[0]);
                if (result[0] == "")
                {
                    refreshCube();
                }
            },        
            onFailure: function(result)
            {
                refreshCube();
                ACE.FaultHandlerDialog.show
                    ({
                    title:   "Warning",
                    message: "Error from Salesforce", 
                    error:   result.faultstring
                    });
            }
        });
    }

    /**
	 * Throws error and returns true if there are dupes. 
	 */
    function handleDupes() 
    {
        var dupes = getDupes();
        if (dupes == null || dupes.length <= 0) 
        {
            return false; 
        }

        displayDupeErrorMessage();
        return true; 
    }

    /**
	 * Get a list of dupes in the grid.
	 */
    function getDupes() 
    {
        gridItems = JSON.parse(JSON.stringify(grid.getItems()));
        idToName = {}; 
        var dupeIdSet = new Set();
        var dupeNames = [];

        for (var i = 0; i < gridItems.length; i++)
        {
            var itemId;
            
            if ((cubeDataCache.currentRecordType == 'Account' && gridItems[i].Contact == null) || 
                (cubeDataCache.currentRecordType == 'Contact' && gridItems[i].Account == null))
            {
                continue; 
            }

            cubeDataCache.currentRecordType == 'Contact' ? itemId = gridItems[i].Account.Id : itemId = gridItems[i].Contact.Id;

            // if its in idToName already, it is a dupe.
            if (idToName[itemId] != null && idToName[itemId] != undefined)
            {
                if (!dupeIdSet.has(itemId))
                {
                    dupeIdSet.add(itemId);
                }
            }
            else 
            {
                cubeDataCache.currentRecordType == 'Contact' ? 
                    idToName[itemId] = gridItems[i].Account.Name : 
                    idToName[itemId] = gridItems[i].Contact.Name;
            }
        }

        // construct list of dupes
        for (var it = dupeIdSet.values(), dupeId= null; dupeId=it.next().value; ) 
        {
            if (idToName[dupeId] != null) 
            {
                dupeNames.push(idToName[dupeId]);
            }
        }
        
        return dupeNames; 
    }

    function handleSaveButton() 
    {
        var deactivateWarningTriggered = false; 

        // only show the deactivated message if it's not in the set of originally active ACRs. 
        deactivatedContactList.forEach(function(a)
        {
            if (originallyActiveACRIdList.has(a) && !deactivateWarningTriggered)
            {
                deactivateWarningTriggered = true; 
                displayDeactivationWarning();
            }
        });

        if (deactivateWarningTriggered)
        {
            return; 
        }

        handleSave();
    }

    function handleCancelButton() 
    {
        refreshCube();
    }

    function refreshCube() 
    {
        sforce.apex.execute("AccountContactRelationCubeController", "getCubeData", {recordId : dataProviderCache.Id, loggedInUserId  : ALM.modelLocator.userId(), loadLimit : currentLimit}, 
        { 
            onSuccess: _token.onSuccess,        
            onFailure: _token.onFailure
        });
    }

    function handleLoadNext1000Button() 
    {
        currentLimit += 1000; 
        sforce.apex.execute("AccountContactRelationCubeController", "getCubeData", {recordId : dataProviderCache.Id, loggedInUserId  : ALM.modelLocator.userId(), loadLimit : currentLimit}, 
        { 
            onSuccess: _token.onSuccess,        
            onFailure: _token.onFailure
        });
    }

    /**
	 * Makes a Error modal and populates it given the error string.
	 */
    function handleFriendlyErrorMessage(errorString)
    {
        errorStringToPrint = "";
        addedRequiredError = false; 
        hasPermissionError = false;

        if (errorString != "") 
        {
            resultSplit = errorString.split('&&&');
            if (errorString.includes("INSUFFICIENT_ACCESS"))
            {
                hasPermissionError = true; 
            }
            for (resultSplitIndex in resultSplit)
            {
                if (resultSplit.hasOwnProperty(resultSplitIndex))
                {
                if (resultSplit[resultSplitIndex].length > 0)
                    {
                        // we only print the required error once. 
                        if (resultSplit[resultSplitIndex].includes("REQUIRED_FIELD_MISSING") && hasPermissionError == false)
                        {
                            if (addedRequiredError)
                            {
                            continue;
                            }
                            addedRequiredError = true; 
                            errorStringToPrint += "Fields highlighted in red are required. Fill in the fields and then save." + '</br>';
                            continue;
                        }
                        if (resultSplit[resultSplitIndex].includes("FIELD_CUSTOM_VALIDATION_EXCEPTION") && hasPermissionError == false)
                        {
                            errorStringToPrint += resultSplit[resultSplitIndex].split("FIELD_CUSTOM_VALIDATION_EXCEPTION,")[1] + '</br>';
                            continue;
                        }
                        
                        // other exceptions
                        errorStringToPrint += resultSplit[resultSplitIndex] + '</br>';
                    }
                }
            }
            if (hasPermissionError)
            {
            ACEConfirm.show
                (
                "You do not have permissions to edit. Contact your administrator.</br>",
                "Warning",
                [ "OK::save-button confirmButton"],
                function(result)
                {
                }
                );
            }
            else 
            {
            ACEConfirm.show
            (
                errorStringToPrint,
                "Warning",
                [ "OK::save-button confirmButton"],
                function(result)
                {
                }
            );
            }
        }
    }

    function displayDupeErrorMessage()
    {
        dupesString = "The contact you are trying to add already exists, please try adding another contact.";
        // Primary Contact cannot be added as an affiliate, please try adding another contact ????
        if (cubeDataCache.currentRecordType == "Contact") 
        {
            dupesString = "Secondary Affiliate Client cannot be added again as it already exists, please try adding another client."
        }
        ACEConfirm.show
        (
            dupesString,
            "Duplicate Error",
            [ "OK::save-button confirmButton"],
            function(result)
            {
            }
        );  
    }

    function displayDeactivationWarning()
    {
        ACEConfirm.show
        (
            "Are you sure you want to inactivate selected relationship records?",
            "Please Confirm",
            [ "Yes::save-button confirmButton", "No::el-btn-lg el-btn-outline"],
            function(result)
            {
                if(result == 'Yes')
                {
                    handleSave();
                }
            }
        );  
    }

    function displayNoWriteAccessWarning()
    {
        ACEConfirm.show
        (
            "You do not have permissions to edit this relation.",
            "Warning",
            [ "OK::save-button confirmButton"],
            function(result)
            {
            }
        );  
    }

    function commitACRCGridChanges()
    {
        grid.grid.getEditorLock().commitCurrentEdit();
        grid.grid.invalidate(); 
    }

    $.extend(true, window,
    {
    "Custom":
    {
        "AccountContactRelationCube": AccountContactRelationCube
    },
    "PS":
    {
        "AccountContactRelationCubeHelpers":
        {
        "updateUpdateList": updateUpdateList,
        "displayNoWriteAccessWarning": displayNoWriteAccessWarning,
        "commitACRCGridChanges": commitACRCGridChanges
        }
    }		
    });

    /**
	 * Polyfills
	 */

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/includes
    if (!String.prototype.includes) {
        Object.defineProperty(String.prototype, 'includes', {
            value: function(search, start) {
            if (typeof start !== 'number') {
                start = 0
            }
            
            if (start + search.length > this.length) {
                return false
            } else {
                return this.indexOf(search, start) !== -1
            }
            }
        })
    }
    // https://tc39.github.io/ecma262/#sec-array.prototype.includes
    if (!Array.prototype.includes) {
        Object.defineProperty(Array.prototype, 'includes', {
            value: function(searchElement, fromIndex) {

            if (this == null) {
                throw new TypeError('"this" is null or not defined');
            }

            // 1. Let O be ? ToObject(this value).
            var o = Object(this);

            // 2. Let len be ? ToLength(? Get(O, "length")).
            var len = o.length >>> 0;

            // 3. If len is 0, return false.
            if (len === 0) {
                return false;
            }

            // 4. Let n be ? ToInteger(fromIndex).
            //    (If fromIndex is undefined, this step produces the value 0.)
            var n = fromIndex | 0;

            // 5. If n ≥ 0, then
            //  a. Let k be n.
            // 6. Else n < 0,
            //  a. Let k be len + n.
            //  b. If k < 0, let k be 0.
            var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

            function sameValueZero(x, y) {
                return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
            }

            // 7. Repeat, while k < len
            while (k < len) {
                // a. Let elementK be the result of ? Get(O, ! ToString(k)).
                // b. If SameValueZero(searchElement, elementK) is true, return true.
                if (sameValueZero(o[k], searchElement)) {
                return true;
                }
                // c. Increase k by 1. 
                k++;
            }

            // 8. Return false
            return false;
            }
        });
    }

    if (!String.prototype.startsWith) {
        Object.defineProperty(String.prototype, 'startsWith', {
            value: function(search, pos) {
                return this.substr(!pos || pos < 0 ? 0 : +pos, search.length) === search;
            }
        });
    }
//# sourceURL=AccountContactRelationCube.js 
})(jQuery);