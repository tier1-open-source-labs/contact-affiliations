/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *  Version: 1.6.3

 */
@isTest
private class TestAccountContactRelationCubeController {

	private static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();

    private static User testUser;
    private static Account a1;
    private static Account a2;
    private static Account a3;
    private static Contact c1;
    private static Contact c2;
    private static Contact c3;
    private static AccountContactRelation acr1;
    private static AccountContactRelation acr2;
    private static String updateString;
    private static String updateString2; 

    private static void setup()
    {
        // Make AFRs.
        dataFactory.createCBSAFR();
        T1C_FR__Feature__c ACEExtensions 																		= new T1C_FR__Feature__c(Name = 'Extensions', T1C_FR__Name__c = 'ACE.Extensions');
		T1C_FR__Feature__c ACEExtensionsAccountContactRelationCube 												= new T1C_FR__Feature__c(Name = 'AccountContactRelationCube', T1C_FR__Name__c = 'ACE.Extensions.AccountContactRelationCube');
		T1C_FR__Feature__c ACEExtensionsAccountContactRelationCubeFilterSettings 								= new T1C_FR__Feature__c(Name = 'FilterSettings', T1C_FR__Name__c = 'ACE.Extensions.AccountContactRelationCube.FilterSettings');
		T1C_FR__Feature__c ACEExtensionsAccountContactRelationCubeGridSettings 							        = new T1C_FR__Feature__c(Name = 'GridSettings', T1C_FR__Name__c = 'ACE.Extensions.AccountContactRelationCube.GridSettings');
		T1C_FR__Feature__c ACEExtensionsAccountContactRelationCubeHeaderColumns				                    = new T1C_FR__Feature__c(Name = 'HeaderColumns', T1C_FR__Name__c = 'ACE.Extensions.AccountContactRelationCube.HeaderColumns');
		T1C_FR__Feature__c ACEExtensionsAccountContactRelationCubeHeaderColumnsAccountName 	                    = new T1C_FR__Feature__c(Name = 'AccountName', T1C_FR__Name__c = 'ACE.Extensions.AccountContactRelationCube.HeaderColumns.AccountName');
		T1C_FR__Feature__c ACEExtensionsAccountContactRelationCubeHeaderColumnsActive 	                        = new T1C_FR__Feature__c(Name = 'Active', T1C_FR__Name__c = 'ACE.Extensions.AccountContactRelationCube.HeaderColumns.Active');
		T1C_FR__Feature__c ACEExtensionsAccountContactRelationCubeHeaderColumnsContactName 				        = new T1C_FR__Feature__c(Name = 'ContactName', T1C_FR__Name__c = 'ACE.Extensions.AccountContactRelationCube.HeaderColumns.ContactName');
		T1C_FR__Feature__c ACEExtensionsAccountContactRelationCubeHeaderColumnsEndDate 		                    = new T1C_FR__Feature__c(Name = 'EndDate', T1C_FR__Name__c = 'ACE.Extensions.AccountContactRelationCube.HeaderColumns.EndDate');
		T1C_FR__Feature__c ACEExtensionsAccountContactRelationCubeHeaderColumnsRoles                            = new T1C_FR__Feature__c(Name = 'Roles', T1C_FR__Name__c = 'ACE.Extensions.AccountContactRelationCube.HeaderColumns.Roles');
        T1C_FR__Feature__c ACEExtensionsAccountContactRelationCubeHeaderColumnsStartDate                        = new T1C_FR__Feature__c(Name = 'StartDate', T1C_FR__Name__c = 'ACE.Extensions.AccountContactRelationCube.HeaderColumns.StartDate');
		
        List<T1C_FR__Feature__c> afrList = new List<T1C_FR__Feature__c> 
		{ 
			ACEExtensions,
			ACEExtensionsAccountContactRelationCube,
			ACEExtensionsAccountContactRelationCubeFilterSettings,
			ACEExtensionsAccountContactRelationCubeGridSettings,
			ACEExtensionsAccountContactRelationCubeHeaderColumns,
			ACEExtensionsAccountContactRelationCubeHeaderColumnsAccountName,
			ACEExtensionsAccountContactRelationCubeHeaderColumnsActive,
			ACEExtensionsAccountContactRelationCubeHeaderColumnsContactName,
			ACEExtensionsAccountContactRelationCubeHeaderColumnsEndDate,
			ACEExtensionsAccountContactRelationCubeHeaderColumnsRoles,
			ACEExtensionsAccountContactRelationCubeHeaderColumnsStartDate
		};
        insert afrList;

        List<T1C_FR__Attribute__c> attr = new List<T1C_FR__Attribute__c>{
        new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeFilterSettings.Id, Name = 'IsOpen', T1C_FR__Value__c = 'TRUE'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeFilterSettings.Id, Name = 'ShowInactive', T1C_FR__Value__c = 'TRUE'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeGridSettings.Id, Name = 'OrderByDirection', T1C_FR__Value__c = 'DESC'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeGridSettings.Id, Name = 'OrderByField', T1C_FR__Value__c = 'Name'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsAccountName.Id, Name = 'DataField', T1C_FR__Value__c = 'Account.Name'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsAccountName.Id, Name = 'FieldType', T1C_FR__Value__c = 'AUTOCOMPLETE'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsAccountName.Id, Name = 'Label', T1C_FR__Value__c = 'Name'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsAccountName.Id, Name = 'Order', T1C_FR__Value__c = '1'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsAccountName.Id, Name = 'Visible', T1C_FR__Value__c = 'TRUE'),
        new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsActive.Id, Name = 'DataField', T1C_FR__Value__c = 'IsActive'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsActive.Id, Name = 'FieldType', T1C_FR__Value__c = 'CHECKBOX'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsActive.Id, Name = 'Label', T1C_FR__Value__c = 'Active'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsActive.Id, Name = 'Order', T1C_FR__Value__c = '20'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsActive.Id, Name = 'Visible', T1C_FR__Value__c = 'TRUE'),
        new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsContactName.Id, Name = 'DataField', T1C_FR__Value__c = 'Contact.Name'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsContactName.Id, Name = 'FieldType', T1C_FR__Value__c = 'AUTOCOMPLETE'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsContactName.Id, Name = 'Label', T1C_FR__Value__c = 'Name'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsContactName.Id, Name = 'Order', T1C_FR__Value__c = '1'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsContactName.Id, Name = 'Visible', T1C_FR__Value__c = 'TRUE'),
        new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsEndDate.Id, Name = 'DataField', T1C_FR__Value__c = 'EndDate'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsEndDate.Id, Name = 'FieldType', T1C_FR__Value__c = 'DATE'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsEndDate.Id, Name = 'Label', T1C_FR__Value__c = 'End Date'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsEndDate.Id, Name = 'Order', T1C_FR__Value__c = '6'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsEndDate.Id, Name = 'Visible', T1C_FR__Value__c = 'EndDate'),
        new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsRoles.Id, Name = 'DataField', T1C_FR__Value__c = 'Roles'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsRoles.Id, Name = 'FieldType', T1C_FR__Value__c = 'MULTIPICKLIST'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsRoles.Id, Name = 'Label', T1C_FR__Value__c = 'Roles'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsRoles.Id, Name = 'Order', T1C_FR__Value__c = '10'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsRoles.Id, Name = 'Visible', T1C_FR__Value__c = 'TRUE'),
        new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsStartDate.Id, Name = 'DataField', T1C_FR__Value__c = 'StartDate'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsStartDate.Id, Name = 'FieldType', T1C_FR__Value__c = 'DATE'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsStartDate.Id, Name = 'Label', T1C_FR__Value__c = 'Start Date'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsStartDate.Id, Name = 'Order', T1C_FR__Value__c = '5'),
		new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACEExtensionsAccountContactRelationCubeHeaderColumnsStartDate.Id, Name = 'Visible', T1C_FR__Value__c = 'TRUE')
        };
        insert attr;

        // Make user
        Profile sysProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].get(0);
            
        testUser = new User(username ='Test1asddvxzcvzxcvQF@test.ASSDFASDFASDF.com',LastName='Test1@test.com', Email='Test1@test.com', Alias='T1', TimeZoneSidKey='GMT', LocaleSidKey='en_US', 
        EmailEncodingKey='ISO-8859-1', ProfileId=sysProfile.Id, LanguageLocaleKey='en_US');
        insert testUser;

        // make some fake data and insert it. 
        a1 = dataFactory.makeAccount('testA1');
        a2 = dataFactory.makeAccount('testA2');
        a3 = dataFactory.makeAccount('testA3');
        insert new list<Account>{a1, a2, a3};

        c1 = dataFactory.makeContact(a1.Id, 'FirstName1', 'LastName1');
        c2 = dataFactory.makeContact(a2.Id, 'FirstName2', 'LastName2');
        c3 = dataFactory.makeContact(a3.Id, 'FirstName3', 'LastName3');
        insert new list<Contact>{c1, c2, c3};

        acr1 = new AccountContactRelation( AccountId = a1.Id, ContactId = c3.Id);
        acr2 = new AccountContactRelation( AccountId = a3.Id, ContactId = c1.Id);
        insert new list<AccountContactRelation>{acr1, acr2};

        // stringified IDs
        String acr1String = String.valueOf(acr1.Id);
        String acr2String = String.valueOf(acr2.Id);
        String a2String = String.valueOf(a2.Id);
        String c2String = String.valueOf(c2.Id);

         updateString = '{' 
                            + '"' + acr1String + '":'
                                        + '{"IsActive":false, "StartDate":"2019-01-15", "EndDate":"2019-01-16" },'
                            + '"new_0":'
                                        + '{"ContactId":"' + c2String + '"}' 
                        + '}';                    // contactId required. accountId = currentRecordId
        
        updateString2 =  '{' 
                            + '"' + acr2String + '":'
                                        + '{"IsActive":false, "StartDate":"2019-01-15", "EndDate":"2019-01-16" },'
                            + '"new_0":'
                                        + '{"AccountId":"' + a2String + '"}' 
                        + '}';                 
    }
    
    static testMethod void basicTest() {
        if (!Schema.getGlobalDescribe().containsKey('AccountContactRelation'))
        {
            System.debug('AccountContactRelation disabled.');
            return;
        }
        setup();

        String a1String = String.valueOf(a1.Id);
        String a2String = String.valueOf(a2.Id);
        String a3String = String.valueOf(a3.Id);
        String c1String = String.valueOf(c1.Id);
        String c2String = String.valueOf(c2.Id);
        Id recordId = a1.Id;
        String currentRecordType = 'Account';
        String serializedCurrentRecordType = JSON.serialize(currentRecordType);
        Id loggedInUserId = testUser.Id;
        Integer loadLimit = 1000; 

        Test.startTest();

        System.debug(AccountContactRelationCubeController.getConfig());
        AccountContactRelationCubeController.updateFilterButtonState(loggedInUserId, 'true');
        AccountContactRelationCubeController.updateFilterButtonState(loggedInUserId, 'false');
        System.debug(AccountContactRelationCubeController.getCubeData(recordId, loggedInUserId, loadLimit));
        System.debug(AccountContactRelationCubeController.updateAccountContactRelations(recordId, serializedCurrentRecordType, updateString));

        // checks if updateString1 worked properly. 
        // query for acr1 and check if it is now false. and start date = 2019-01-15 and endDate = 2019-01-16
        // query for c2 + a1 and check if it exists. 
        AccountContactRelation check1 = [SELECT Id, AccountId, ContactId, IsActive, StartDate, EndDate FROM AccountContactRelation WHERE AccountId = :a1String AND ContactId = :c2String];
        System.assertEquals(check1.IsActive, true);
        System.assertEquals(check1.AccountId, a1.Id);
        System.assertEquals(check1.ContactID, c2.Id);

        AccountContactRelation check2 = [SELECT Id, AccountId, ContactId, IsActive, StartDate, EndDate FROM AccountContactRelation WHERE Id = :acr1.Id];
        Date sDate = Date.valueOf('2019-01-15');
        Date eDate = Date.valueOf('2019-01-16');
        System.assertEquals(check2.IsActive, false);
        System.assertEquals(check2.StartDate, sDate);
        System.assertEquals(check2.EndDate, eDate);

        // checks for updatestring2
        recordId = c1.Id;
        currentRecordType = 'Contact';
        serializedCurrentRecordType = JSON.serialize(currentRecordType);

        System.debug(AccountContactRelationCubeController.getConfig());
        System.debug(AccountContactRelationCubeController.getCubeData(recordId, loggedInUserId, loadLimit));
        System.debug(AccountContactRelationCubeController.updateAccountContactRelations(recordId, serializedCurrentRecordType, updateString2));

        AccountContactRelation check3 = [SELECT Id, AccountId, ContactId, IsActive, StartDate, EndDate FROM AccountContactRelation WHERE Id = :acr2.Id];
        System.assertEquals(check3.IsActive, false);
        System.assertEquals(check3.StartDate, sDate);
        System.assertEquals(check3.EndDate, eDate);
        
        AccountContactRelation check4 = [SELECT Id, AccountId, ContactId, IsActive, StartDate, EndDate FROM AccountContactRelation WHERE AccountId = :a2String AND ContactId = :c1String];
        System.assertEquals(check4.IsActive, true);
        System.assertEquals(check4.AccountId, a2.Id);
        System.assertEquals(check4.ContactID, c1.Id);

        Test.stopTest();
    }

    // Insert it and confirm that the new/updated relations are updated properly. 
}