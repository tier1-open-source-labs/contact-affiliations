/** 
 * Name: AccountContactRelationCubeController 
 * Version: 1.6.3
 * Description: Controller for the Account Contact Relation Cube
 *
 * Confidential & Proprietary, 2019 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */

global with sharing class AccountContactRelationCubeController 
{
    private static String ACCOUNT_CONTACT_RELATION_CUBE_FEATURE_PATH = 'ACE.Extensions.AccountContactRelationCube';
    public static T1C_FR.Feature accountContactRelationFeature
    {
        get
        {
            if (accountContactRelationFeature == null)
            {
                accountContactRelationFeature = T1C_FR.FeatureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), ACCOUNT_CONTACT_RELATION_CUBE_FEATURE_PATH);
            }
            return accountContactRelationFeature;
        }
    }

    public static List<String> getACRCPicklistValues(String dataField) 
    {
        list<String> picklistOptions = new list<String>();
        SObjectUtil.DynamicUIPicklistConfig o = new SObjectUtil.DynamicUIPicklistConfig();
        picklistOptions = o.getPicklistValues('AccountContactRelation', dataField);

        return picklistOptions;
    }

    WebService static string getConfig() 
    {
        return JSON.serialize(new AccountContactRelationCubeConfig());
    }

    WebService static AccountContactRelationCubeData getCubeData(Id recordId, Id loggedInUserId, Integer loadLimit) 
    {
        String orderByField;
        String orderByDirection;

        // determine which queries get called. 
        String currentRecordType = recordId.getSObjectType().getDescribe().getName();

        // get the cube related AFR data. 
        for (T1C_FR.Feature accountContactRelationFeatureChild : accountContactRelationFeature.SubFeatures)
        {
            if (accountContactRelationFeatureChild.Name == ACCOUNT_CONTACT_RELATION_CUBE_FEATURE_PATH + '.GridSettings')
            {
                orderByField = AFRUtil.getAttributeValue(accountContactRelationFeatureChild, 'OrderByField', 'Name');
                orderByDirection = AFRUtil.getAttributeValue(accountContactRelationFeatureChild, 'OrderByDirection', 'ASC');
            }
        }

        // get the relation data. 
        if (currentRecordType == 'Contact')
        {
            return new AccountContactRelationCubeData(loggedInUserId, currentRecordType, recordId, getIndirectAccounts(recordId, loadLimit), orderByField, orderByDirection);
        }
        else if (currentRecordType == 'Account')
        {
            return new AccountContactRelationCubeData(loggedInUserId, currentRecordType, recordId, getIndirectContacts(recordId, loadLimit), orderByField, orderByDirection);
        }
        return null;
    }

    /**
     *    Given a userId, and a list of account contact relations, return a list of relations that the user has write access to. 
     */
    public static list<Id> getRelationsWithWriteAccess(Id loggedInUserId, list<AccountContactRelation> accountContactRelations)
    {
        // get contacts that user has access to.
        Set<Id> contactIdsWithWriteAccess = new Set<Id>();
        List<Id> contactIds = new List<Id>();
        for (AccountContactRelation acr : accountContactRelations)
        {
            contactIds.add(acr.Contact.Id);
        }

        List<Contact> contacts = [
            SELECT Id, UserRecordAccess.HasEditAccess
            FROM Contact 
            WHERE Id IN :contactIds
            ];
        
        for (Contact c: contacts)
        {
            if (c.UserRecordAccess.HasEditAccess)
            {
                contactIdsWithWriteAccess.add(c.Id);
            }
        }

        // get relations with write access. 
        List<Id> relationIdsWithWriteAccess = new List<Id>(); 
        for (AccountContactRelation acr : accountContactRelations)
        {
            if (contactIdsWithWriteAccess.contains(acr.Contact.Id))
            {
                relationIdsWithWriteAccess.add(acr.Id);
            }
        }
        return relationIdsWithWriteAccess;
    }

    /**
     * Given a contact's ID - returns the contact's relations. 
     */
    public static list<AccountContactRelation> getIndirectAccounts(Id recordId, Integer loadLimit) 
    {
        list<AccountContactRelation> indirectAccounts = new list<AccountContactRelation>();

        // // get fields
        // String sobject_type = 'AccountContactRelation';
        // Map<String, Schema.SObjectType> global_describe = Schema.getGlobalDescribe();
        // Map<String, Schema.SObjectField> object_fields_map = global_describe.get(sobject_type).getDescribe().fields.getMap();
        // String fieldsToQuery = 'Account.Name, Contact.Name, Contact.HasOptedOutOfEmail';
        // for (String objectField : object_fields_map.keySet())
        // {
        //     // https://developer.salesforce.com/forums/?id=906F00000009024IAA
        //     fieldsToQuery += ', ' + object_fields_map.get(objectField);
        // }

        // System.debug('test1');
        // System.debug(fieldsToQuery);

        String fieldsToQuery = 'Contact.HasOptedOutOfEmail';
        List<String> fields = getFieldsToQuery();
        for (String field : fields)
        {
            fieldsToQuery += ', ' + field;
        }

        String soqlQuery = 'select ' + fieldsToQuery + ' from AccountContactRelation where ContactId =: recordID  and IsDirect = false LIMIT :loadLimit';
        indirectAccounts = database.query(soqlQuery);

        return indirectAccounts;
    }

    /**
     * Given a account's ID - returns the account's relations. 
     */
    public static list<AccountContactRelation> getIndirectContacts(Id recordId, Integer loadLimit) 
    {
        list<AccountContactRelation> indirectContacts = new list<AccountContactRelation>();

        String fieldsToQuery = 'Contact.HasOptedOutOfEmail';
        List<String> fields = getFieldsToQuery();
        for (String field : fields)
        {
            fieldsToQuery += ', ' + field;
        }

        if (loadLimit == -1)
        {
            String soqlQuery = 'select ' + fieldsToQuery + ' from AccountContactRelation where AccountId =: recordID  and IsDirect = false';
            indirectContacts = database.query(soqlQuery);
        }
        else
        {
            String soqlQuery = 'select ' + fieldsToQuery + ' from AccountContactRelation where AccountId =: recordID  and IsDirect = false LIMIT :loadLimit';
            indirectContacts = database.query(soqlQuery);            
        }
        
        return indirectContacts;
    }

    public static List<String> getFieldsToQuery()
    {
        List<String> fields = new List<String>(); 
        // get fields
        String sobject_type = 'AccountContactRelation';
        Map<String, Schema.SObjectType> global_describe = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> object_fields_map = global_describe.get(sobject_type).getDescribe().fields.getMap();
        for (String objectField : object_fields_map.keySet())
        {
            // https://developer.salesforce.com/forums/?id=906F00000009024IAA
            if (!fields.contains('' + object_fields_map.get(objectField)))
            {
                fields.add('' + object_fields_map.get(objectField));
            }
        }

        // get additional fields from the AFRs. 
        T1C_FR.Feature accountContactRelationHeaderColumnsFeature = T1C_FR.FeatureCacheWebSvc.getAttributeTreeRecursive(UserInfo.getUserId(), ACCOUNT_CONTACT_RELATION_CUBE_FEATURE_PATH  + '.HeaderColumns');
        for (T1C_FR.Feature HeaderColumnsChild : accountContactRelationHeaderColumnsFeature.SubFeatures)
        {
            String dataField = AFRUtil.getAttributeValue(HeaderColumnsChild, 'DataField');
            if (!fields.contains(dataField))
            {
                fields.add(dataField);
            }
        }

        return fields; 
    }

    /**
     * Given an Account Id, Contact Id, map of changes, updates it and returns an error string. 
     */
    public static String updateAccountContactRelation(String relationId, Map<String,String> changesMap, String currentRecordTypeDeserialized, Id currentRecordIdId, Map<ID, AccountContactRelation> relationMap)
    {
        String errorString = '';
        String sobject_type = 'AccountContactRelation';
        Boolean isNewRelation = false; 
        Map<String, Schema.SObjectType> global_describe = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> object_fields_map = global_describe.get(sobject_type).getDescribe().fields.getMap();

        system.debug(relationId);
        Pattern newIdPattern = Pattern.compile('new_\\d*'); 
        Matcher newIdPatternMatcher = newIdPattern.matcher(relationId);

        // get relation object if it exists. 
        AccountContactRelation relation; 
        if (newIdPatternMatcher.matches())
        {
            if (currentRecordTypeDeserialized == 'Account')
            {
                // check if the Account Contact relation already exists. If it does, use that. 
                String cID = changesMap.get('ContactId');
                try
                {
                    relation = [
                        select Id, AccountId, ContactId
                        from AccountContactRelation 
                        where ContactId = :cID AND AccountId = :currentRecordIdId
                        ];
                    relation.IsActive = true; 
                    relation.EndDate = null; 
                }
                catch (QueryException e)
                {
                    // relation doesn't exist - make a new one
                    relation = new AccountContactRelation();
                    relation.ContactId = cID;
                    relation.AccountId = currentRecordIdId;
                }
            }
            else if (currentRecordTypeDeserialized == 'Contact')
            {
                // check if the Account Contact relation already exists. If it does, use that. 
                String aID = changesMap.get('AccountId');
                try
                {
                    relation = [
                        select Id, AccountId, ContactId
                        from AccountContactRelation 
                        where ContactId = :currentRecordIdId AND AccountId = :aID
                        ];
                    relation.IsActive = true; 
                    relation.EndDate = null; 
                }
                catch (QueryException e)
                {
                    // relation doesn't exist - make a new one. 
                    relation = new AccountContactRelation();
                    relation.ContactId = currentRecordIdId;
                    relation.AccountId = aID;
                }
            }
        }
        else
        {
            // not a new relation. 
            try 
            {
                if (relationMap.containsKey(relationId))
                {
                    relation = relationMap.get(relationId);
                }
                else 
                {
                    relation = new AccountContactRelation();
                    isNewRelation = true; 
                }
            } 
            catch (Exception e)
            {
                system.debug('[ACRC] not in relation map?');
            }
        }

        // add changes to relation.  
        for (String field : changesMap.keySet()) 
        {
            String fieldContent = changesMap.get(field);

            // when reactivating a relation, reset the enddate to blank or user entered value. 
            if (field == 'IsActive') 
            {
                if (fieldContent == 'true') 
                {
                    if (!isNewRelation)
                    {
                        system.debug(relation.IsActive);
                    }
                    relation.IsActive = true;
                } 
                else if (fieldContent == 'false') 
                {
                    relation.IsActive = false;
                    if (changesMap.get('EndDate') == null) // if the user didnt set an enddate, make it today. 
                    {
                        relation.EndDate = Date.today(); 
                    }
                }
            }
            else if (field == 'ContactId' || field == 'AccountId')
            {
                continue;
            }
            else
            {
                Schema.SObjectField sObjField = object_fields_map.get(field);
                if (sObjField.getDescribe().getType() == Schema.DisplayType.Boolean)
                {
                    relation.put(field, Boolean.valueOf(fieldContent));
                } 
                else if (sObjField.getDescribe().getType() == Schema.DisplayType.ID)
                {
                    relation.put(field, ID.valueOf(fieldContent));
                }
                else if (sObjField.getDescribe().getType() == Schema.DisplayType.Date)
                {
                    if (fieldContent == null)
                    {
                        relation.put(field, null);
                    }
                    else
                    {
                        relation.put(field, Date.valueOf(fieldContent));
                    }
                }
                else 
                {
                    relation.put(field, (fieldContent));
                }
            }
            system.debug('updated relations content: ');
            system.debug(relation);
        }

        try 
        {
            upsert relation;
        }
        catch(DmlException e)
        {
            // permission exceptions are dml exceptions - we parse the string later using delimiter '&&&' 
            errorString += e.getDmlMessage(0) + '&&&'; 
        }

        return errorString;
    }

    WebService static String updateAccountContactRelations(String currentRecordId, String currentRecordType, String updateList)
    {
        System.debug(updateList);

        String errorString = '';

        // Clean the inputs
        String currentRecordIdClean = currentRecordId.replaceAll('"','');
        String currentRecordTypeDeserialized = (String)JSON.deserialize(currentRecordType, String.class);
        Id currentRecordIdId = Id.valueOf(currentRecordIdClean);
        Map<String,Map<String,String>> deserializedUpdateList = (Map<String,Map<String,String>>)JSON.deserialize(updateList, Map<String,Map<String,String>>.class);

        // Make a map of changes - relationMap = id:relation map
        Map<ID, AccountContactRelation> relationMap;
        if (currentRecordTypeDeserialized == 'Contact') 
        {
            relationMap = new Map<ID, AccountContactRelation>([
                    select Id, ContactId, AccountId, IsActive, IsDirect, EndDate, StartDate, Roles, CreatedBy.UserName
                    from AccountContactRelation
                    where ContactId =: currentRecordIdId]);
        }
        else if (currentRecordTypeDeserialized == 'Account') 
        {
            relationMap = new Map<ID, AccountContactRelation>([
                    select Id, ContactId, AccountId, IsActive, IsDirect, EndDate, StartDate, Roles, CreatedBy.UserName
                    from AccountContactRelation
                    where AccountId =: currentRecordIdId]);
        }

        // go through each relation and update it - the expected size of the array is very small. 
        for (String relationId : deserializedUpdateList.keySet()) 
        {
            system.debug(relationId);
            errorString += updateAccountContactRelation(relationId, deserializedUpdateList.get(relationId), currentRecordTypeDeserialized, currentRecordIdId, relationMap);
        }
        System.debug(errorString);
        return errorString;
    }

    /** 
     * Given a mapping of the path + order value, update the AFRs. 
     */
    Webservice static void reorderColumns(Id userId, String attributeMap) 
    {
        Map<String,String> deserializedattributeMap = (Map<String,String>)JSON.deserialize(attributeMap, Map<String,String>.class);
        System.debug(deserializedattributeMap);

        for (String path : deserializedattributeMap.keySet())
        {
            List<String> pathParts = path.split('\\.');
            T1C_FR.FeatureCacheWebSvc.setUserAttribute(userId, path, 'Order', deserializedattributeMap.get(path));
        }
    }

    /** 
     * Given a mapping pf path to width, update the AFRs. 
     */
    Webservice static void updateColumnWidth(Id userId, String featureToWidth)
    {
        Map<String,String> featureToWidthDeserialized = (Map<String,String>)JSON.deserialize(featureToWidth, Map<String,String>.class);
        System.debug(featureToWidthDeserialized);

        for (String path : featureToWidthDeserialized.keySet())
        {
            List<String> pathParts = path.split('\\.');
            T1C_FR.FeatureCacheWebSvc.setUserAttribute(userId, path, 'Width', featureToWidthDeserialized.get(path));
        }
    }
    
    Webservice static void updateFilterButtonState(Id userId, String isOpen) 
    {
        String isOpenDeserialized = (String)JSON.deserialize(isOpen, String.class);
        String isOpenCaps = isOpenDeserialized.toUpperCase();

        T1C_FR.FeatureCacheWebSvc.setUserAttribute(userId, 'ACE.Extensions.AccountContactRelationCube.FilterSettings', 'IsOpen', isOpenCaps);
    }

    /** 
     * Name: AccountContactRelationCubeConfig
     * Description: Wrapper class for slickgrid column configs.  
     */
    global class AccountContactRelationCubeColumnConfig 
    {
        public String name;
        public String label;
        public String dataField;
        public String fieldType;
        public Integer order;
        public Integer width; 
        public Boolean visible;
        public String enabledTypes; 
        public Boolean readOnly;
        public String featurePath;
        public List<String> picklistOptions;

        public AccountContactRelationCubeColumnConfig(T1C_FR.Feature feature)
        {
            this.featurePath = feature.Name; 
            this.label = AFRUtil.getAttributeValue(feature, 'Label');
            this.dataField = AFRUtil.getAttributeValue(feature, 'DataField');
            this.fieldType = AFRUtil.getAttributeValue(feature, 'FieldType', 'TEXT');
            this.order = AFRUtil.getIntegerAttributeValue(feature, 'Order', -1);
            this.width = AFRUtil.getIntegerAttributeValue(feature, 'Width', 100);
            this.visible = AFRUtil.getBooleanAttributeValue(feature, 'Visible', false);
            this.enabledTypes = AFRUtil.getAttributeValue(feature, 'EnabledTypes', 'ALL');
            this.readOnly = AFRUtil.getBooleanAttributeValue(feature, 'ReadOnly', false);
            if (fieldType == 'PICKLIST' || fieldType == 'MULTIPICKLIST')
            {
                this.picklistOptions = getACRCPicklistValues(dataField);
            }
        }
    }

    /** 
     * Name: AccountContactRelationCubeConfig
     * Description: Wrapper class for cube configs.  
     */
    global class AccountContactRelationCubeConfig 
    {
        public List<AccountContactRelationCubeColumnConfig> columnConfigs;
        public Boolean showInactive;
        public Boolean isOpen;
        public String cancelButtonLabel;
        public String filterTextPlaceholderLabel;
        public String loadButtonLabel;
        public String newButtonLabel;
        public String saveButtonLabel;
        public String showInactiveLabel;
        public String primaryAccountSelectedErrorMessage;
        public String primaryContactSelectedErrorMessage;

        public AccountContactRelationCubeConfig()
        {
            // iterate through the parent AFR and handle each child. (the sub AFRs)
            columnConfigs = new List<AccountContactRelationCubeColumnConfig>();
            for (T1C_FR.Feature accountContactRelationFeatureChild : accountContactRelationFeature.SubFeatures)
            {
                if (accountContactRelationFeatureChild.Name == ACCOUNT_CONTACT_RELATION_CUBE_FEATURE_PATH + '.HeaderColumns')
                {
                    for (T1C_FR.Feature columnFeature : accountContactRelationFeatureChild.SubFeatures)
                    {
                        AccountContactRelationCubeColumnConfig newColumnConfig = new AccountContactRelationCubeColumnConfig(columnFeature);
                        this.columnConfigs.add(newColumnConfig);
                    }
                }
                else if (accountContactRelationFeatureChild.Name == ACCOUNT_CONTACT_RELATION_CUBE_FEATURE_PATH + '.FilterSettings')
                {
                    this.showInactive = AFRUtil.getBooleanAttributeValue(accountContactRelationFeatureChild, 'ShowInactive', false);
                    this.isOpen = AFRUtil.getBooleanAttributeValue(accountContactRelationFeatureChild, 'IsOpen', false);
                }
                else if (accountContactRelationFeatureChild.Name == ACCOUNT_CONTACT_RELATION_CUBE_FEATURE_PATH + '.Labels')
                {
                    this.cancelButtonLabel = AFRUtil.getAttributeValue(accountContactRelationFeatureChild, 'CancelButtonLabel', 'Cancel');
                    this.filterTextPlaceholderLabel = AFRUtil.getAttributeValue(accountContactRelationFeatureChild, 'FilterTextPlaceholderLabel', 'Enter text to filter any columns');
                    this.loadButtonLabel = AFRUtil.getAttributeValue(accountContactRelationFeatureChild, 'LoadButtonLabel', 'Load Next 1000');
                    this.newButtonLabel = AFRUtil.getAttributeValue(accountContactRelationFeatureChild, 'NewButtonLabel', 'New');
                    this.saveButtonLabel = AFRUtil.getAttributeValue(accountContactRelationFeatureChild, 'SaveButtonLabel', 'Save');
                    this.showInactiveLabel = AFRUtil.getAttributeValue(accountContactRelationFeatureChild, 'ShowInactiveLabel', 'Show Inactive');
                    this.primaryAccountSelectedErrorMessage = AFRUtil.getAttributeValue(accountContactRelationFeatureChild, 'PrimaryAccountSelectedErrorMessage', 
                        'Primary Client cannot be added as a Secondary Affiliate, please try adding another client.');
                    this.primaryContactSelectedErrorMessage = AFRUtil.getAttributeValue(accountContactRelationFeatureChild, 'PrimaryContactSelectedErrorMessage', 
                        'Primary Contact cannot be added as a Secondary Affiliate, please try adding another contact.');
                }
            }
        }
    }

    /** 
     * Name: AccountContactRelationCubeData
     * Description: Wrapper class that holds all the cube data.
     */
    global class AccountContactRelationCubeData 
    {
        WebService list<AccountContactRelation> indirectAccounts = null;
        WebService list<AccountContactRelation> indirectContacts = null;
        WebService String currentRecordType;
        WebService Id currentRecordId;
        WebService list<Id> relationsWithWriteAccess;
        WebService String sortField;
        WebService String sortDirection;

        global AccountContactRelationCubeData(Id loggedInUserId, String recordType, String recordId, list<SObject> records, String orderByField, String orderByDirection)
        {
            currentRecordType = recordType;
            currentRecordId = recordId;
            if (recordType == 'Contact') 
            {
                indirectAccounts = records;
            } 
            else if (recordType == 'Account') 
            {
                indirectContacts = records;
            }
            relationsWithWriteAccess = getRelationsWithWriteAccess(loggedInUserId, records);
            sortField = orderByField;
            sortDirection = orderByDirection;
        }
    }
}